﻿using FactorsBoostBot.MonitorMobileProxy.CTypes;
using Medallion.Shell;
using Microsoft.Extensions.Configuration;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serializers.SystemTextJson;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Console = Colorful.Console;

namespace FactorsBoostBot.MonitorMobileProxy
{
	class ModemTools
	{
		private static Regex regexBalanceMegafon = new Regex(@"'([0-9.\-]+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
		private static Regex regexQuotes = new Regex(@"'(.+)'", RegexOptions.Compiled | RegexOptions.IgnoreCase);
		private static Regex regexIp = new Regex(@": (.+)$", RegexOptions.Compiled | RegexOptions.IgnoreCase);
		private static Regex regexTemp = new Regex(@": ([0-9.,]+)$", RegexOptions.Compiled | RegexOptions.IgnoreCase);
		private static Regex regexSignal = new Regex(@": '(.+)'", RegexOptions.Compiled | RegexOptions.IgnoreCase);
		private static Regex regexModem = new Regex(@"^/org/freedesktop/ModemManager(\d+)/Modem/(\d+)$", RegexOptions.Compiled | RegexOptions.IgnoreCase);
		private static Regex regexDhcpObtained = new Regex(@"udhcpc: lease of ([0-9.]+) obtained", RegexOptions.Compiled | RegexOptions.IgnoreCase);
		private static IConfiguration configuration;
		private static object lockerDb = new object();
		private static readonly Shell CommandShell = new Shell(options => options.ThrowOnError(false).Timeout(TimeSpan.FromSeconds(5)));

		/// <summary>
		/// Возвращает инфу об интерфейсах.
		/// </summary>
		/// <returns></returns>
		public static List<CIpIface> listInterfaces()
		{
			List<CIpIface> listIface = new List<CIpIface>();

			foreach (NetworkInterface iface in NetworkInterface.GetAllNetworkInterfaces())
			{
				listIface.Add(new CIpIface {
					ifname = iface.Id,
					status = iface.OperationalStatus
				});
			}
			return listIface;
		}

		/// <summary>
		/// Возвращает номера модемов из ModemManager.
		/// </summary>
		/// <returns></returns>
		public static async Task<List<int>> GetListNumModemsFromManager()
		{
			List<int> result = new List<int>();
			List<string> outputCmd = new List<string>();
			CommandResult resCmd = null;
			try
			{
				resCmd = await CommandShell.Run("/usr/bin/mmcli", new string[] { "-L", "--output-json" }).RedirectTo(outputCmd).Task;
			}
			catch { }
			if (resCmd != null && resCmd.Success)
			{
				string jsonStr = string.Join("", outputCmd);
				CMManagerModemList resModems = JsonSerializer.Deserialize<CMManagerModemList>(jsonStr);
				if (resModems != null && resModems.modemList != null && resModems.modemList.Count > 0)
				{
					foreach (string patchModem in resModems.modemList)
					{
						Match matchNum = regexModem.Match(patchModem);
						if (matchNum.Success) result.Add(Convert.ToInt32(matchNum.Groups[2].Value));
					}
				}
			}
			return result;
		}

		/// <summary>
		/// Возвращает информацию о модеме из ModemManager.
		/// </summary>
		/// <param name="numModem">Номер модема</param>
		/// <returns></returns>
		public static async Task<CMManagerModemInfo> GetInfoModem(int numModem)
		{
			List<string> outputCmd = new List<string>();
			CommandResult resCmd = null;
			CMManagerModemInfo resModem = null;

			try
			{
				resCmd = await CommandShell.Run("/usr/bin/mmcli", new string[] { $"--modem={numModem}", "--output-json" }).RedirectTo(outputCmd).Task;
			}
			catch { }

			if (resCmd != null && resCmd.Success)
			{
				string jsonStr = string.Join("", outputCmd);
				resModem = JsonSerializer.Deserialize<CMManagerModemInfo>(jsonStr);
			}

			return resModem;
		}

		/// <summary>
		/// Возвращает информацию о SIM карте.
		/// </summary>
		/// <param name="numModem">Номер модема</param>
		/// <param name="pathSim">Путь к SIM карте</param>
		/// <returns></returns>
		public static async Task<CMManagerSimInfo> GetInfoSim(int numModem, string pathSim)
		{
			List<string> outputCmd = new List<string>();
			CommandResult resCmd = null;
			CMManagerSimInfo resSim = null;

			try
			{
				resCmd = await CommandShell.Run("/usr/bin/mmcli", new string[] { $"--modem={numModem}", $"--sim={pathSim}", "--output-json" }).RedirectTo(outputCmd).Task;
			}
			catch { }

			if (resCmd != null && resCmd.Success)
			{
				string jsonStr = string.Join("", outputCmd);
				resSim = JsonSerializer.Deserialize<CMManagerSimInfo>(jsonStr);
			}

			return resSim;
		}

		/// <summary>
		/// Возвращает статус модема
		/// </summary>
		/// <param name="modem"></param>
		/// <returns></returns>
		public static async Task<string> GetState(CModem modem)
		{
			CMManagerModemInfo result = await GetInfoModem(modem.modemManagerId);
			try
			{
				return result.modem.generic.state;
			}
			catch
			{
				return null;
			}
		}

		/// <summary>
		/// Включает модем.
		/// </summary>
		/// <param name="numModem"></param>
		/// <returns></returns>
		public static async Task EnableModem(int numModem)
		{
			try
			{
				await CommandShell.Run("/usr/bin/mmcli", new string[] { $"--modem={numModem}", "--enable" }).Task;
			}
			catch { }
		}

		/// <summary>
		/// Получает информацию о соединении модема.
		/// </summary>
		/// <param name="modem"></param>
		/// <param name="bearerPath"></param>
		/// <returns></returns>
		public static async Task<CMManagerBearerInfoBearer> GetBearersByPath(int modemId, string bearerPath)
		{
			CMManagerBearerInfoBearer result = null;
			List<string> outputCmd = new List<string>();
			CommandResult resCmd = null;
			try
			{
				resCmd = await CommandShell.Run("/usr/bin/mmcli", new string[] { $"--modem={modemId}", $"--bearer={bearerPath}", "--output-json" }).RedirectTo(outputCmd).Task;
			}
			catch { }
			if (resCmd != null && resCmd.Success)
			{
				string jsonStr = string.Join("", outputCmd);
				CMManagerBearerInfo resModem = JsonSerializer.Deserialize<CMManagerBearerInfo>(jsonStr);
				if (resModem != null && resModem.bearer != null)
				{
					result = resModem.bearer;
				}
			}
			return result;
		}

		public static async Task<List<CItemRouteTable>> GetListSystemTableRoute()
		{
			List<CItemRouteTable> res = new List<CItemRouteTable>();

			List<string> outputCmd = new List<string>();
			CommandResult resCmd = null;
			try
			{
				//resCmd = await CommandShell.Run(
				//	"/usr/bin/ip",
				//	Array.Empty<object>(),
				//	options: o => o.StartInfo(si => si.Arguments = $"-j route list table all")
				//).RedirectTo(outputCmd).Task;
				resCmd = await CommandShell.Run("/usr/bin/ip", new string[] { "-j", "route", "list", "table", "all" }).RedirectTo(outputCmd).Task;
			}
			catch { }

			if (resCmd != null && resCmd.Success)
			{
				res = JsonSerializer.Deserialize<List<CItemRouteTable>>(string.Join("", outputCmd));
			}

			return res;
		}

		/// <summary>
		/// Удаляет соединение.
		/// </summary>
		/// <param name="modem"></param>
		/// <returns></returns>
		public static async Task DeleteBearer(int modemId, string bearerPath)
		{
			try
			{
				await CommandShell.Run("/usr/bin/mmcli", new string[] { $"--modem={modemId}", $"--delete-bearer={bearerPath}" }).Task;
			}
			catch { }
		}

		/// <summary>
		/// Создает и возвращает соединение с настройками провайдера.
		/// </summary>
		/// <param name="modem"></param>
		/// <returns></returns>
		public static async Task<CMManagerBearerInfoBearer> CreateBearers(int numModem, CSettingOperator settingOperator)
		{
			Console.WriteLine($"Пробуем для модема {numModem}, добавить соединение {settingOperator.apn}");
			CMManagerBearerInfoBearer result = null;
			List<string> outputCmd = new List<string>();
			CommandResult resCmd = null;
			try
			{
				if (settingOperator.apnLogin == null)
				{
					//resCmd = await CommandShell.Run("/usr/bin/mmcli", new string[] { $"--modem={numModem}", $"--create-bearer='apn={settingOperator.apn},allow-roaming=no,ip-type=ipv4'" }).RedirectTo(outputCmd).Task;
					resCmd = await CommandShell.Run(
						"/usr/bin/mmcli",
						Array.Empty<object>(),
						options: o => o.StartInfo(si => si.Arguments = $"--modem={numModem} --create-bearer=\"apn={settingOperator.apn},allowed-auth=chap,allow-roaming=no,ip-type=ipv4\"")
					).RedirectTo(outputCmd).Task;
				}
				else
				{
					//resCmd = await CommandShell.Run("/usr/bin/mmcli", new string[] { $"--modem={numModem}", $"--create-bearer=\"apn={settingOperator.apn},user={settingOperator.apnLogin},password={settingOperator.apnPassword},allowed-auth=chap,allow-roaming=no,ip-type=ipv4\"" }).RedirectTo(outputCmd).Task;
					resCmd = await CommandShell.Run(
						"/usr/bin/mmcli", 
						Array.Empty<object>(),
						options: o => o.StartInfo(si => si.Arguments = $"--modem={numModem} --create-bearer=\"apn={settingOperator.apn},user={settingOperator.apnLogin},password={settingOperator.apnPassword},allowed-auth=chap,allow-roaming=no,ip-type=ipv4\"")
					).RedirectTo(outputCmd).Task;
				}
			}
			catch { }

			if (resCmd != null && resCmd.Success && outputCmd.Count > 0 && outputCmd[0].Contains("Successfully"))
			{
				string bearerPath = outputCmd[1].Trim();
				result = await GetBearersByPath(numModem, bearerPath);
			}

			return result;
		}

		/// <summary>
		/// Перегружает модем.
		/// </summary>
		/// <param name="modem"></param>
		/// <returns></returns>
		public static async Task Reboot(CModem modem, bool isNeedRemoveModem = true)
		{
			try
			{
				ProxyTools proxy = ProxyTools.GetInstance();
				ListModems listModems = ListModems.GetInstance();
				proxy.SetConfiguration(configuration);

				if (IsNotNull(modem.uid))
				{
					modem.active = false;
					if (modem.port3proxy > 0 && isNeedRemoveModem) proxy.RemovePort(modem.port3proxy);
					modem.port3proxy = 0;
					modem.host3proxy = null;
					modem.wan = null;
					modem.ping = null;
					modem.state = "reboot";
					modem.signalStrength = null;
					modem.trafficStats = null;
					modem.bearer = null;

					if (isNeedRemoveModem)
					{
						listModems.Set(modem);
						// Правим файл конфига 3proxy
						await proxy.Update3Proxy();
					}

					// echo "AT^RESET" | socat - /dev/ttyUSB7,crnl
					// ./RunAtCommand.sh "AT^RESET" /dev/ttyUSB3
					Console.WriteLine($"Перезагрузка модема {modem.dev}", Color.Red);
					if (IsNotNull(modem.comPort)) await CommandShell.Run($"{AppContext.BaseDirectory}BashScripts/RunAtCommand.sh", new string[] { "AT+QPOWD=1", modem.comPort }).Task;
					else await CommandShell.Run("/usr/bin/mmcli", new string[] { $"--modem={modem.modemManagerId}", "--reset" }).Task;

					//CommandResult resCmd = await CommandShell.Run(
					//	$"{AppContext.BaseDirectory}BashScripts/RunAtCommand.sh",
					//	Array.Empty<object>(),
					//	options: o => o.StartInfo(si => si.Arguments = $"\"AT^RESET\" {modem.comPort}")
					//).Task;

					//await CommandShell.Run("/usr/bin/mmcli", new string[] { $"--modem={modem.modemManagerId}", "--reset" }).Task;
				}
			}
			catch { }
		}

		/// <summary>
		/// Сменяет у модема ip.
		/// </summary>
		/// <param name="modem"></param>
		/// <returns></returns>
		public static async Task<CModem> ChangeIp(CModem modem)
		{
			ProxyTools proxy = ProxyTools.GetInstance();
			ListModems listModems = ListModems.GetInstance();
			proxy.SetConfiguration(configuration);

			if (IsNotNull(modem.uid))
			{
				modem.active = false;
				proxy.RemovePort(modem.port3proxy);
				modem.port3proxy = 0;
				modem.host3proxy = null;
				modem.wan = null;
				modem.ping = null;
				modem.state = "change_ip";

				listModems.Set(modem);

				// Правим файл конфига 3proxy
				await proxy.Update3Proxy();

				await Reboot(modem, false);

				//if (modem.operatorName == "Beeline")
				//{
				//	await Reboot(modem);
				//}
				//else
				//{
				//	// Отключимся от интернета
				//	await DisconnectInternet(modem.modemManagerId, modem.bearer.dbusPath);
				//	await Task.Delay(500);
				//	await CheckModem(modem.modemManagerId, configuration);
				//}

				// Правим файл конфига 3proxy
				await proxy.Update3Proxy();
			}


			return listModems.GetByUid(modem.uid);
		}

		public static void SetConfiguration(IConfiguration conf)
		{
			configuration = conf;
		}

		public static bool IsNotNull(string str)
		{
			return !string.IsNullOrWhiteSpace(str) && str != "--";
		}

		public async static Task CheckModem(int numModem, IConfiguration configuration)
		{
			ProxyTools proxy = ProxyTools.GetInstance();
			ListModems listModems = ListModems.GetInstance();

			Dictionary<string, CSettingOperator> settingsOperators = new Dictionary<string, CSettingOperator>();
			configuration.GetSection("SettingsOperators").Bind(settingsOperators);

			int steps = 0;

			bool isNeedExit = false;
			
			while (!isNeedExit)
			{
				// Получим информацию о модеме
				CMManagerModemInfo resModem = await GetInfoModem(numModem);
				if (resModem != null && resModem.modem != null)
				{
					// Наченем собирать информацию о модеме
					CModem modem = new CModem();
					modem.modemManagerId = numModem;
					modem.id = 0;
					modem.active = false;
					modem.city = configuration["City"];
					modem.type = "Mobile";

					if (resModem.modem.generic != null)
					{
						if (IsNotNull(resModem.modem.generic.deviceIdentifier))
						{
							modem.uid = resModem.modem.generic.deviceIdentifier;
							// Строковый id у нас есть, получим числовой
							lock (lockerDb)
							{
								using (LiteDB.LiteDatabase db = new LiteDB.LiteDatabase("modems.db"))
								{
									LiteDB.ILiteCollection<CModemUid> col = db.GetCollection<CModemUid>("modems_uid");
									col.EnsureIndex(x => x.uid);
									CModemUid findUid = col.FindOne(x => x.uid.Contains(modem.uid));
									if (findUid != null)
									{
										modem.id = findUid.id;
									}
									else
									{
										col.Insert(new CModemUid { uid = modem.uid });
										findUid = col.FindOne(x => x.uid.Contains(modem.uid));
										modem.id = findUid.id;
									}
								}
							}
						}

						if (IsNotNull(resModem.modem.generic.state)) modem.state = resModem.modem.generic.state;
						if (IsNotNull(resModem.modem.generic.equipmentIdentifier)) modem.imei = resModem.modem.generic.equipmentIdentifier;
						if (IsNotNull(resModem.modem.generic.primaryPort)) modem.dev = "/dev/" + resModem.modem.generic.primaryPort;
						if (IsNotNull(resModem.modem.generic.manufacturer)) modem.manufacturer = resModem.modem.generic.manufacturer;
						if (IsNotNull(resModem.modem.generic.model)) modem.model = resModem.modem.generic.model;
						if (IsNotNull(resModem.modem.generic.hardwareRevision)) modem.revision = resModem.modem.generic.hardwareRevision;
						if (resModem.modem.generic.signalQuality != null) modem.signalQuality = resModem.modem.generic.signalQuality.value;


						if (resModem.modem.generic.ownNumbers != null && resModem.modem.generic.ownNumbers.Count > 0) modem.numPhone = resModem.modem.generic.ownNumbers[0];
						if (resModem.modem.generic.ports != null && resModem.modem.generic.ports.Count > 0)
						{
							modem.wwanIface = resModem.modem.generic.ports.Where(p => p.Contains("(net)")).FirstOrDefault().Replace("(net)", "").Trim();
							// Поищем AT COM порт
							string atComPort = resModem.modem.generic.ports.Where(p => p.Contains("(at)")).LastOrDefault()?.Replace("(at)", "").Trim();
							if (IsNotNull(atComPort))
							{
								modem.comPort = "/dev/" + atComPort;
							}
						}

						if (IsNotNull(resModem.modem.generic.sim))
						{
							// Получим информацию о SIM карте
							CMManagerSimInfo resSim = await GetInfoSim(numModem, resModem.modem.generic.sim);
							if (resSim != null && resSim.sim != null && resSim.sim.properties != null)
							{
								if (IsNotNull(resSim.sim.properties.imsi)) modem.imsi = resSim.sim.properties.imsi;
								if (IsNotNull(resSim.sim.properties.iccid)) modem.iccid = resSim.sim.properties.iccid;
								if (IsNotNull(resSim.sim.properties.operatorName)) modem.operatorName = resSim.sim.properties.operatorName;
								if (IsNotNull(resSim.sim.properties.operatorCode)) modem.operatorCode = resSim.sim.properties.operatorCode;
							}

							/*if (modem.imei != null && modem.imsi != null)
							{
								// У нас есть и imei и imsi, получим id модема
								using (MD5 md5Hash = MD5.Create())
								{
									//From String to byte array
									byte[] hashBytes = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(modem.imei + ":" + modem.imsi));
									modem.id = BitConverter.ToString(hashBytes).Replace("-", string.Empty);
								}
							}*/

							if (IsNotNull(modem.iccid))
							{
								lock (lockerDb)
								{
									using (LiteDB.LiteDatabase db = new LiteDB.LiteDatabase("modems.db"))
									{
										LiteDB.ILiteCollection<CSimOptions> colSim = db.GetCollection<CSimOptions>("sim_options");
										colSim.EnsureIndex(x => x.iccid);
										modem.simOptions = colSim.FindOne(x => x.iccid.Contains(modem.iccid));
									}
								}
							}
						}
					}

					if (resModem.modem.i3gpp != null)
					{
						if (IsNotNull(resModem.modem.i3gpp.operatorName)) modem.operatorName = resModem.modem.i3gpp.operatorName;
						if (IsNotNull(resModem.modem.i3gpp.operatorCode)) modem.operatorCode = resModem.modem.i3gpp.operatorCode;
					}

					// Получим настройки именно оператора
					if (IsNotNull(modem.operatorName) && settingsOperators.ContainsKey(modem.operatorName)) modem.settingOperator = settingsOperators[modem.operatorName];

					// Получим настроки соединения с интернетом
					if (resModem.modem.generic.bearers != null && resModem.modem.generic.bearers.Count > 0)
					{
						modem.bearer = await GetBearersByPath(numModem, resModem.modem.generic.bearers[0]);
					}

					// Найдем модем в общем списке
					CModem findModem = null;
					if (IsNotNull(modem.uid)) findModem = listModems.GetByUid(modem.uid);

					// Получение данных если есть соединение с интернетом
					if (modem.dev != null && modem.state == "connected" && modem.bearer != null)
					{
						// Получим IP из полученных ранее настроек
						modem.wan = GetNetworkInfo(modem.bearer);

						// Получим внешний IP
						if (modem.wan != null && IsNotNull(modem.wan.ip))
						{
							//Console.WriteLine($"Модем {modem.dev}: не получен внешний IP", Color.Red);


							// Такого модема в списке еще нет, значит получим внешний IP
							if (findModem == null) modem.wan.extIp = await GetExtIp(modem.wan.ip, modem.wan.gateway, modem.wwanIface);
							else // Такой модем в списке есть
							{
								// Если в списке уже есть внутренний ip и он совпадает с текущими найстройками
								if (findModem.wan != null && IsNotNull(findModem.wan.ip) && findModem.wan.ip == modem.wan.ip && IsNotNull(findModem.wan.extIp)) modem.wan.extIp = findModem.wan.extIp;
								else modem.wan.extIp = await GetExtIp(modem.wan.ip, modem.wan.gateway, modem.wwanIface);
							}

							// Проверим таблицу маршрутизации
							await CheckRouteTableModem(modem.wan.ip, modem.wan.gateway, modem.wwanIface);
						}

						// Уровень сигнала
						modem.signalStrength = await GetModemSignalStrength(modem.dev);
						// Сколько пакетом скачано, отдано
						modem.trafficStats = GetTrafficStats(modem.bearer, modem.trafficStats);
					}

					// Такой модем есть в списке и у него есть активный прокси порт
					if (findModem != null && findModem.port3proxy > 0) modem.port3proxy = findModem.port3proxy;
					if (findModem != null && findModem.balance != null) modem.balance = findModem.balance;
					if (findModem != null && findModem.ping != null) modem.ping = findModem.ping;

					if (IsNotNull(modem.comPort)) modem.temperature = await GetModemTemperature(modem);

					// Если нет инфы о пинге соединения модема, произведем его
					if (modem.bearer != null && modem.state == "connected" && modem.wan != null && IsNotNull(modem.wan.ip) && modem.balance != null && modem.balance > 0 && modem.ping == null)
					{
						modem.ping = await HttpPing.Start(IPAddress.Parse(modem.wan.ip));
					}

					//ObjectDump.Write(Console.Out, findModem);

					//Console.WriteLine();
					//Console.WriteLine($"ID {(modem.id > 0 ? modem.id : null)}", Color.White);
					//Console.WriteLine($"numModem {(numModem > 0 ? numModem : null)}", Color.White);
					//Console.WriteLine($"UID {(modem.uid != null ? modem.uid : null)}", Color.White);
					//Console.WriteLine($"Модем {(modem.dev != null ? modem.dev : null)}", Color.White);
					//Console.WriteLine($"Интерфейс {(modem.wwanIface != null ? modem.wwanIface : null)}", Color.White);
					//Console.WriteLine($"COM порт {(modem.comPort != null ? modem.comPort : null)}", Color.White);
					//Console.WriteLine($"IMEI {(modem.imei != null ? modem.imei : null)}", Color.White);
					//Console.WriteLine($"IMSI {(modem.imsi != null ? modem.imsi : null)}", Color.White);
					//Console.WriteLine($"ICCID {(modem.iccid != null ? modem.iccid : null)}", Color.White);
					//Console.WriteLine($"Производитель {(modem.manufacturer != null ? modem.manufacturer : null)}", Color.White);
					//Console.WriteLine($"Модель {(modem.model != null ? modem.model : null)}", Color.White);
					//Console.WriteLine($"Оператор {(modem.operatorName != null ? modem.operatorName : null)}", Color.White);
					//Console.WriteLine($"Ревизия {(modem.revision != null ? modem.revision : null)}", Color.White);
					//Console.WriteLine($"Номер телефона {(modem.numPhone != null ? modem.numPhone : null)}", Color.White);
					//Console.WriteLine($"IP {(modem.wan != null ? modem.wan.ip : null)}", Color.White);
					//Console.WriteLine($"Внешний IP {(modem.wan != null ? modem.wan.extIp : null)}", Color.White);
					//Console.WriteLine($"Статус соединения {(modem.state != null ? modem.state : null)}", Color.White);
					//Console.WriteLine($"Порт 3proxy {modem.port3proxy}", Color.White);

					//Console.WriteLine($"Качество сигнала {modem.signalQuality}", Color.White);
					//Console.WriteLine($"RSSI {(modem.signalStrength != null ? modem.signalStrength.rssi : null)}", Color.White);
					//Console.WriteLine($"ECIO {(modem.signalStrength != null ? modem.signalStrength.ecio : null)}", Color.White);
					//Console.WriteLine($"IO {(modem.signalStrength != null ? modem.signalStrength.io : null)}", Color.White);
					//Console.WriteLine($"SINR {(modem.signalStrength != null ? modem.signalStrength.sinr : null)}", Color.White);
					//Console.WriteLine($"SNR {(modem.signalStrength != null ? modem.signalStrength.snr : null)}", Color.White);
					//Console.WriteLine($"RSRQ {(modem.signalStrength != null ? modem.signalStrength.rsrq : null)}", Color.White);
					//Console.WriteLine($"RSRP {(modem.signalStrength != null ? modem.signalStrength.rsrp : null)}", Color.White);
					//Console.WriteLine($"registrationState  {(modem.signalStrength != null ? modem.signalStrength.registrationState : null)}", Color.White);
					//Console.WriteLine($"С каким провайдером соединились {(modem.signalStrength != null ? modem.signalStrength.network : null)}", Color.White);
					//Console.WriteLine($"mcc {(modem.signalStrength != null ? modem.signalStrength.mcc : null)}", Color.White);
					//Console.WriteLine($"mnc {(modem.signalStrength != null ? modem.signalStrength.mnc : null)}", Color.White);
					//Console.WriteLine($"nameOperator {(modem.signalStrength != null ? modem.signalStrength.nameOperator : null)}", Color.White);
					//Console.WriteLine($"cellId {(modem.signalStrength != null ? modem.signalStrength.cellId : null)}", Color.White);
					//Console.WriteLine($"trackingAreaCode {(modem.signalStrength != null ? modem.signalStrength.trackingAreaCode : null)}", Color.White);
					//Console.WriteLine($"locationAreaCode {(modem.signalStrength != null ? modem.signalStrength.locationAreaCode : null)}", Color.White);
					//Console.WriteLine($"Режим передачи данных {(modem.signalStrength != null ? modem.signalStrength.mode : null)}", Color.White);
					//Console.WriteLine($"Доступные частоты {(modem.signalStrength != null ? string.Join(", ", modem.signalStrength.bandPreference) : null)}", Color.White);
					//Console.WriteLine($"агрегация {(modem.signalStrength != null ? string.Join(", ", modem.signalStrength.lteBand) : null)}", Color.White);

					//Console.WriteLine($"TX bytes {(modem.trafficStats != null ? modem.trafficStats.bytesTx : null)}", Color.White);
					//Console.WriteLine($"RX bytes {(modem.trafficStats != null ? modem.trafficStats.bytesRx : null)}", Color.White);
					//Console.WriteLine($"Скорость TX {(modem.trafficStats != null ? modem.trafficStats.speedBpsTx : null)}", Color.White);
					//Console.WriteLine($"Скорость RX {(modem.trafficStats != null ? modem.trafficStats.speedBpsRx : null)}", Color.White);

					//Console.WriteLine($"Температура {(modem.temperature != null ? string.Join(", ", modem.temperature) : null)}", Color.White);
					//Console.WriteLine($"Баланс {(modem.balance != null ? modem.balance : null)} ", Color.White);

					//Console.WriteLine();


					// Теперь главные проверки и включение модема в работу

					// Не удалось получить dev устройство
					if (modem.dev == null)
					{
						Console.WriteLine($"Модем {numModem}: не удалось получить dev устройство.", Color.Red);
						isNeedExit = true;
					}
					else if (modem.state == "failed")
					{
						Console.WriteLine($"Модем {modem.dev}: ошибка: {resModem.modem.generic.stateFailedReason}.", Color.Red);
						isNeedExit = true;
					}
					else if (!IsNotNull(modem.uid))
					{
						Console.WriteLine($"Модем {modem.dev}: не удалось получить id устройства.", Color.Red);
						isNeedExit = true;
					}
					// Модем не активирован?
					// Включим модем
					else if (modem.state == "disabled")
					{
						Console.WriteLine($"Модем {modem.dev}: включаем.", Color.Green);
						await EnableModem(numModem);
					}
					// У модема не правильно выставлены режимы работы?
					else if (resModem.modem.generic.currentModes != "allowed: 3g, 4g; preferred: 4g")
					{
						Console.WriteLine($"Модем {modem.dev}: выставляем режим работы 3g, 4g.", Color.Green);
						await SetModeModem(numModem);
					}
					// Не удалось получить wwanIface интерфейс
					else if (modem.wwanIface == null)
					{
						Console.WriteLine($"Модем {modem.dev}: не удалось получить wwanIface интерфейс.", Color.Red);
						isNeedExit = true;
					}
					// Неизвестен провайдер
					else if (modem.operatorName == null)
					{
						Console.WriteLine($"Модем {modem.dev}: не удалось определить провайдера.", Color.Red);
						isNeedExit = true;
					}
					// не подключена антенна
					else if (modem.signalQuality < -100)
					{
						Console.WriteLine($"Модем {modem.dev}: не подключена антенна.", Color.Red);
						isNeedExit = true;
					}
					// не обнаружена SIM карта
					else if (modem.imsi == null)
					{
						Console.WriteLine($"Модем {modem.dev}: не обнаружена SIM карта.", Color.Red);
						isNeedExit = true;
					}
					// У модема нет соединений
					else if (modem.bearer == null && modem.settingOperator != null)
					{
						Console.WriteLine($"Модем {modem.dev}: нет настроек соединения, добавляем их.", Color.Green);
						await CreateBearers(numModem, modem.settingOperator);
					}
					// Получим баланс. Запрашиваем только если он не был ранее получен. Переодически запрос делает другая задача.
					else if ((modem.state == "registered" || modem.state == "connected") && modem.simOptions != null && modem.simOptions.mode == "ussd" && ((findModem != null && findModem.balance == null) || findModem == null))
					{
						Console.WriteLine($"Модем {modem.dev}: получим баланс sim карты через USSD.", Color.Green);
						modem.balance = await GetBalanceByUSSD(modem);
					}
					// Получим баланс. Запрашиваем только если он не был ранее получен. Переодически запрос делает другая задача.
					else if ((modem.state == "registered" || modem.state == "connected") && modem.simOptions != null && modem.simOptions.mode == "web" && ((findModem != null && findModem.balance == null) || findModem == null))
					{
						Console.WriteLine($"Модем {modem.dev}: получим баланс sim карты через web кабинет.", Color.Green);
						modem.balance = await GetBalanceByWeb(modem);
					}
					// Если несколько соединений
					else if (modem.bearer != null && resModem.modem.generic.bearers != null && resModem.modem.generic.bearers.Count > 1)
					{
						Console.WriteLine($"Модем {modem.dev}: неправильные настройки соединения - больше одного, удаляем все.", Color.Red);
						// Удалим это не правильное соединение
						foreach (string b in resModem.modem.generic.bearers) await DeleteBearer(numModem, b);
					}
					// Проверим правильность настроек подключения
					else if (modem.bearer != null && modem.bearer.properties.apn != modem.settingOperator.apn)
					{
						Console.WriteLine($"Модем {modem.dev}: неправильные настройки соединения.", Color.Red);
						// Удалим это не правильное соединение
						await DeleteBearer(numModem, resModem.modem.generic.bearers[0]);
					}
					// Модем еще не подлючен к интернету
					else if (modem.bearer != null && modem.state == "registered")
					{
						Console.WriteLine($"Модем {modem.dev}: не имеет соединения с интернетом. Соединяем.", Color.Green);

						// Установим режим модема
						// qmicli --device=/dev/cdc-wdm0 -p --set-expected-data-format=raw-ip
						Console.WriteLine($"Модем {modem.dev}: устанавливаем режим raw-ip.", Color.Green);
						await SetModemExpectedDataFormatRawIp(modem.dev);

						// Включим сетевой интерфейс
						Console.WriteLine($"Модем {modem.dev}: включаем интерфейс {modem.wwanIface}.", Color.Green);
						await UpIface(modem.wwanIface);
						// Подключимся к интернету
						Console.WriteLine($"Модем {modem.dev}: подключаемся к интернету.", Color.Green);
						await ConnectInternet(modem.modemManagerId, modem.bearer.dbusPath);
						// Получим IP через DHCP
						Console.WriteLine($"Модем {modem.dev}: получаем IP по DHCP.", Color.Green);
						modem.isNeedGetIpDhcp = !(await GetIpByDhcp(modem));
					}
					else if (modem.bearer != null && modem.state == "connected" && modem.wan != null && IsNotNull(modem.wan.ip) && !IsNotNull(modem.wan.extIp))
					{
						Console.WriteLine($"Модем {modem.dev}: нет внешнего IP.", Color.Red);
						// Получим IP через DHCP
						Console.WriteLine($"Модем {modem.dev}: получаем внутренний IP по DHCP.", Color.Green);
						modem.isNeedGetIpDhcp = !(await GetIpByDhcp(modem));
						modem.wan.extIp = await GetExtIp(modem.wan.ip, modem.wan.gateway, modem.wwanIface);
					}
					else if (modem.bearer != null && modem.state == "connected" && modem.isNeedGetIpDhcp && (modem.wan == null || !IsNotNull(modem.wan.ip)))
					{
						Console.WriteLine($"Модем {modem.dev}: не удалось получить IP. Пробуем еще раз.", Color.Red);
						// Получим IP через DHCP
						Console.WriteLine($"Модем {modem.dev}: получаем IP по DHCP.", Color.Green);
						modem.isNeedGetIpDhcp = !(await GetIpByDhcp(modem));
					}
					else if (modem.bearer != null && modem.state == "connected" && modem.balance != null && modem.balance < 0)
					{
						Console.WriteLine($"Модем {modem.dev}: отрицательный баланс! {modem.balance}.", Color.Red);
						modem.active = false;
						isNeedExit = true;
					}
					else if (modem.bearer != null && modem.state == "connected" && modem.isNeedGetIpDhcp && modem.wan != null && !IsNotNull(modem.wan.ip) && modem.balance != null && modem.balance > 0)
					{
						modem.isNeedGetIpDhcp = false;
						// Правим таблицу маршрутизации
						Console.WriteLine($"Модем {modem.dev}: правим таблицу маршрутизации.", Color.Green);
						await CheckRouteTableModem(modem.wan.ip, modem.wan.gateway, modem.wwanIface);
					}
					//else if (modem.bearer != null && modem.state == "connected" && modem.wan != null && IsNotNull(modem.wan.ip) && IsNotNull(modem.wan.extIp) && modem.balance != null && modem.balance > 0 && modem.ping != null && modem.ping.cntGoodPing < 3)
					//{
					//	Console.WriteLine($"Модем {modem.dev}: подключение успешно, пока тестируем канал данных. Прошло удачных {modem.ping.cntGoodPing} пингов.", Color.Yellow);
					//	isNeedExit = true;
					//}
					else if (modem.bearer != null && modem.state == "connected" && modem.wan != null && IsNotNull(modem.wan.ip) && IsNotNull(modem.wan.extIp) && modem.balance != null && modem.balance > 0/* && modem.ping != null && modem.ping.cntGoodPing >= 3*/)
					{
						//Console.WriteLine($"Модем {modem.dev}: подключение успешно, модем доступен для использования.", Color.Green);
						modem.port3proxy = proxy.GetFreePort(modem.wan.ip, modem.operatorName, modem.uid);
						modem.host3proxy = configuration["ServerIp"];

						modem.active = true;
						isNeedExit = true;
					}
					else
					{
						Console.WriteLine($"Модем {modem.dev}: неизвестная ошибка.", Color.Red);
						Console.WriteLine($"Модем {modem.dev}: modem.wan={modem.wan == null}", Color.Red);
						isNeedExit = true;
					}

					// Если socks порт есть, но модем не активный, то удалим порт
					if (modem.port3proxy > 0 && !modem.active)
					{
						proxy.RemovePort(modem.port3proxy);
						modem.port3proxy = 0;
						modem.host3proxy = null;
					}

					//if (!modem.active || modem.wan == null || !IsNotNull(modem.wan.ip))
					//{
					//	modem.wan = null;
					//	modem.ping = null;
					//}

					// Обновим в главном списке модемов
					if (IsNotNull(modem.uid)) listModems.Set(modem);

					steps++;
					if (steps >= 30)
					{
						Console.WriteLine($"Модем {modem.dev}: неизвестная ошибка.", Color.Red);
						isNeedExit = true;
					}
				}
				else isNeedExit = true;
			}
		}

		public static async Task<CModem> GetUpdateInfoWanModem(CModem modem)
		{
			// Обновим данные о соединении
			modem.bearer = await GetBearersByPath(modem.modemManagerId, modem.bearer.dbusPath);
			// Получим IP из полученных ранее настроек
			modem.wan = GetNetworkInfo(modem.bearer);
			// Удалим шлюз модема, чтобы не мешал
			await CheckRouteTableModem(modem.wan.ip, modem.wan.gateway, modem.wwanIface);
			// Получим внешний IP
			modem.wan.extIp = await GetExtIp(modem.wan.ip, modem.wan.gateway, modem.wwanIface);
			// Получим статус модема
			modem.state = await GetState(modem);

			return modem;
		}

		/// <summary>
		/// Возвращает список всех устройств-модемов.
		/// </summary>
		/// <returns></returns>
		public static async Task<List<string>> GetListModemsDev()
		{
			List<string> outputCmd = new List<string>();
			CommandResult resCmd = null;
			resCmd = await CommandShell.Run("/usr/bin/bash", new string[] { "-c", "ls", "/dev/cdc-w*" }).RedirectTo(outputCmd).Task;
			List<string> listTmpDev = new List<string>();
			if (resCmd != null && resCmd.Success) listTmpDev = outputCmd.Where(p => !string.IsNullOrEmpty(p)).ToList();
			return listTmpDev;
		}


		/// <summary>
		/// Переводит модем в режим raw-ip
		/// </summary>
		/// <param name="modem"></param>
		/// <returns></returns>
		public static async Task SetModemExpectedDataFormatRawIp(string dev)
		{
			// qmicli --device=/dev/cdc-wdm0 -p --set-expected-data-format=raw-ip
			try
			{
				await CommandShell.Run("/usr/bin/qmicli", new string[] { $"--device={dev}", "-p", "--set-expected-data-format=raw-ip" }).Task;
			}
			catch { }
		}

		/// <summary>
		/// Устанавливает режим модема 3g/4g и предпочтительный 4g режим.
		/// </summary>
		/// <param name="modem"></param>
		/// <returns></returns>
		public static async Task SetModeModem(int numModem)
		{
			try
			{
				await CommandShell.Run(
					"/usr/bin/mmcli",
					Array.Empty<object>(),
					options: o => o.StartInfo(si => si.Arguments = $"--modem={numModem} --set-allowed-modes='3g|4g' --set-preferred-mode=4g")
				).Task;
			}
			catch { }
		}

		/// <summary>
		/// Включает сетевой интерфейс.
		/// </summary>
		/// <param name="iface"></param>
		/// <returns></returns>
		public static async Task UpIface(string wwanIface)
		{
			try
			{
				if (wwanIface != null)
				{
					// Активируем интерфейс и режим работы raw-ip
					await CommandShell.Run("ip", new string[] { "link", "set", "dev", wwanIface, "down" }).Task;
					await CommandShell.Run("echo", new string[] { $"'Y' > /sys/class/net/{wwanIface}/qmi/raw_ip" }).Task;
					await CommandShell.Run("ip", new string[] { "link", "set", "dev", wwanIface, "up" }).Task;
				}
			}
			catch { }
		}

		/// <summary>
		/// Настраивает модем и соединяет с интернетом.
		/// </summary>
		/// <param name="modem"></param>
		/// <returns></returns>
		public static async Task ConnectInternet(int numModem, string bearerDbusPath)
		{
			try
			{
				await CommandShell.Run("/usr/bin/mmcli", new string[] { $"--modem={numModem}", $"--bearer={bearerDbusPath}", "--connect" }).Task;
				// Установим MTU
				// ip addr add 2.68.206.100/32 dev wwan0
			}
			catch { }
		}

		/// <summary>
		/// Получаем по DHCP IP от провайдера
		/// </summary>
		/// <param name="modem"></param>
		/// <returns></returns>
		public static async Task<bool> GetIpByDhcp(CModem modem)
		{
			try
			{
				if (modem.bearer != null && modem.bearer.dbusPath != null)
				{
					int i = 0;
					while (true)
					{
						i++;
						List<string> outputCmd = new List<string>();
						CommandResult resCmd = null;

						// Получим IP от провайдера
						Console.WriteLine($"Получим IP от провайдера для модема {modem.wwanIface} #udhcpc -A 20 -q -f -n -i {modem.wwanIface}", Color.Yellow);
						// udhcpc -q -f -n -i wwan0
						//List<string> outputCmd = new List<string>();
						// udhcpc -A 20 -q -f -n -i wwan0
						resCmd = await CommandShell.Run("/usr/sbin/udhcpc", new string[] { "-A", "20", "-q", "-f", "-n", "-i", modem.wwanIface }).RedirectTo(outputCmd).Task;

						if (resCmd != null && IsNotNull(resCmd.StandardError))
						{
							// Ошибка аренды
							if (resCmd.StandardError.Contains("udhcpc: no lease, failing")) return false;

							// Получим выданный IP
							Match matchObtained = regexDhcpObtained.Match(resCmd.StandardError);
							if (!(matchObtained.Success && matchObtained.Groups != null && matchObtained.Groups.Count > 0 && IsNotNull(matchObtained.Groups[1].Value))) return false;
						}

						// Обновим данные о соединении
						modem.bearer = await GetBearersByPath(modem.modemManagerId, modem.bearer.dbusPath);
						// Получим IP из полученных ранее настроек
						modem.wan = GetNetworkInfo(modem.bearer);

						if (modem.wan != null) await CheckRouteTableModem(modem.wan.ip, modem.wan.gateway, modem.wwanIface);
						ListModems listModems = ListModems.GetInstance();
						if (IsNotNull(modem.uid)) listModems.Set(modem);


						//modem.ping = await PingTools.PingHost("yandex.ru", IPAddress.Parse(modem.wan.ip), modem.ping != null ? modem.ping.cntBadPing : 0);
						modem.ping = await HttpPing.Start(IPAddress.Parse(modem.wan.ip));
						if (modem.ping.result == 0 || i >= 2)
						{
							if (IsNotNull(modem.uid)) listModems.Set(modem);
							break;
						}
					}
				}
			}
			catch (Exception ex) 
			{
				return false;
			}

			return true;
		}

		public static async Task<bool> CheckErrorConnect(CModem modem)
		{
			if (modem.state == "connected")
			{
				bool isNeedGetDhcp = false;

				if (modem.signalStrength != null)
				{
					if (string.IsNullOrWhiteSpace(modem.signalStrength.trackingAreaCode) || modem.signalStrength.snr == null || modem.signalStrength.rsrq == null || modem.signalStrength.rsrp == null) isNeedGetDhcp = true;
				}

				if (isNeedGetDhcp)
				{
					// Получим новый IP
					await GetIpByDhcp(modem);
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Отключает модем от интернета.
		/// </summary>
		/// <param name="modem"></param>
		/// <returns></returns>
		public static async Task DisconnectInternet(int numModem, string bearerDbusPath)
		{
			try
			{
				await CommandShell.Run("/usr/bin/mmcli", new string[] { $"--modem={numModem}", $"--bearer={bearerDbusPath}", "--disconnect" }).Task;
			}
			catch { }
		}

		/// <summary>
		/// Правит таблицу маршрутизации.
		/// </summary>
		/// <param name="ip"></param>
		/// <returns></returns>
		public static async Task CheckRouteTableModem(string ip, string gateway, string iface)
		{
			try
			{
				Console.WriteLine($"Проверка таблицы маршрутизации интерфейса {iface} шлюза {gateway} ip {ip}", Color.Green);
				// Получим таблицу маршрутизации
				List<CItemRouteTable> tableRoute = await GetListSystemTableRoute();
				// Удалим не правильные маршруты
				if (tableRoute.Where(t => t.dst == "default" && t.dev == iface && t.gateway != gateway).FirstOrDefault() != null)
				{
					Console.WriteLine($"Для интерфейса {iface} удаляем правило", Color.Red);
					await CommandShell.Run("/usr/sbin/route", new string[] { "del", "default", iface }).Task;
					tableRoute = await GetListSystemTableRoute();
				}

				// Если записи дефолтного марштура нет
				if (tableRoute.Where(t => t.dst == "default" && t.dev == iface).FirstOrDefault() == null)
				{
					// Добавим его
					// route add default gw 10.42.192.103 wwan1
					await CommandShell.Run("/usr/sbin/route", new string[] { "add", "default", "gw", gateway, iface }).Task;
					Console.WriteLine($"Добавляем новое правило для интерфейса {iface} gateway {gateway}", Color.Blue);
					tableRoute = await GetListSystemTableRoute();
				}

				// Поправим метрику шлюза
				// ifmetric wwan0 1000
				if (tableRoute.Where(t => t.dst == "default" && t.dev == iface && t.metric != 100).FirstOrDefault() != null) await CommandShell.Run("/usr/sbin/ifmetric", new string[] { iface, "100" }).Task;
			}
			catch { }
		}

		/// <summary>
		/// Возвращает внешний IP модема.
		/// </summary>
		/// <param name="interfaceIp"></param>
		/// <returns></returns>
		public static async Task<string> GetExtIp(string interfaceIp, string interfaceGateway, string interfaceName)
		{
			int i = 0;
			while (true)
			{
				if (i > 5) break;

				Console.WriteLine($"Пробуем для интерфейса {interfaceIp} узнать внешний IP", Color.White);

				await CheckRouteTableModem(interfaceIp, interfaceGateway, interfaceName);

				try
				{
					var client = new RestClient("http://80.90.117.34:34127");
					client.Timeout = 10000;
					client.ConfigureWebRequest((r) =>
					{
						r.ServicePoint.Expect100Continue = false;
						r.KeepAlive = true;
						r.ServicePoint.BindIPEndPointDelegate = (servicePoint, remoteEndPoint, retryCount) => IPEndPoint.Parse(interfaceIp);
					});
					client.UseSystemTextJson();
					client.Authenticator = new HttpBasicAuthenticator("user_check", "af080508n");
					RestRequest request = new RestRequest("infoclient", DataFormat.Json);
					request.AddHeader("Accept", "application/json");
					CInfoFingerprintProxy infoFingerprintProxy = await client.GetAsync<CInfoFingerprintProxy>(request, CancellationToken.None);
					if (infoFingerprintProxy != null && !string.IsNullOrWhiteSpace(infoFingerprintProxy.ip) && !infoFingerprintProxy.ip.Contains("192.168.")) return infoFingerprintProxy.ip;
				}
				catch {}

				i++;
			}

			return null;
		}

		/// <summary>
		/// Возвращает температуру модема.
		/// PMIC - это ИС управления питанием, XO - кварцевый генератор 19,2 M, подключенный к PMIC, а PA - ИС усилителя мощности.
		/// </summary>
		/// <param name="modem"></param>
		/// <returns>pmic температура, xo температура, pa температура</returns>
		public static async Task<List<int>> GetModemTemperature(CModem modem)
		{
			List<int> result = null;

			List<string> outputCmd = new List<string>();
			CommandResult resCmd = null;
			try
			{
				resCmd = await CommandShell.Run(
					$"{AppContext.BaseDirectory}BashScripts/RunAtCommand.sh",
					Array.Empty<object>(),
					options: o => o.StartInfo(si => si.Arguments = $"\"AT+QTEMP\" {modem.comPort}")
				).RedirectTo(outputCmd).Task;
			}
			catch { }

			if (outputCmd.Count > 0)
			{
				string str = outputCmd.Where(s => s.Contains("+QTEMP:")).FirstOrDefault();
				if (IsNotNull(str))
				{
					Match match = regexTemp.Match(str);
					if (match.Success)
					{
						string tmp = match.Groups[1].Value;
						if (IsNotNull(tmp))
						{
							string[] tmpT = tmp.Split(",");
							if (tmpT.Length > 0)
							{
								result = new List<int>();
								foreach (string t in tmpT)
								{
									result.Add(Convert.ToInt32(t));
								}
							}
						}
					}
				}
			}

			return result;
		}

		/// <summary>
		/// Возвращает сетевые настройки выданные провайдером.
		/// </summary>
		/// <param name="modem"></param>
		/// <returns></returns>
		public static CModemNetwork GetNetworkInfo(CMManagerBearerInfoBearer bearer)
		{
			CModemNetwork wan = new CModemNetwork
			{
				ip = null,
				gateway = null,
				subnetmask = null,
				primaryDns = null,
				secondaryDns = null,
				mtu = null
			};

			if (bearer != null && bearer.ipv4Config != null)
			{
				if (IsNotNull(bearer.ipv4Config.address)) wan.ip = bearer.ipv4Config.address;
				if (IsNotNull(bearer.ipv4Config.gateway)) wan.gateway = bearer.ipv4Config.gateway;
				if (IsNotNull(bearer.ipv4Config.mtu)) wan.mtu = Convert.ToInt32(bearer.ipv4Config.mtu);
				if (bearer.ipv4Config.dns != null & bearer.ipv4Config.dns.Count > 0 && IsNotNull(bearer.ipv4Config.dns[0])) wan.primaryDns = bearer.ipv4Config.dns[0];
				if (bearer.ipv4Config.dns != null & bearer.ipv4Config.dns.Count > 1 && IsNotNull(bearer.ipv4Config.dns[1])) wan.secondaryDns = bearer.ipv4Config.dns[1];
				if (IsNotNull(bearer.ipv4Config.prefix))
				{
					switch (Convert.ToInt32(bearer.ipv4Config.prefix))
					{
						case 1:
							wan.subnetmask = "128.0.0.0";
							break;
						case 2:
							wan.subnetmask = "192.0.0.0";
							break;
						case 3:
							wan.subnetmask = "224.0.0.0";
							break;
						case 4:
							wan.subnetmask = "240.0.0.0";
							break;
						case 5:
							wan.subnetmask = "248.0.0.0";
							break;
						case 6:
							wan.subnetmask = "252.0.0.0";
							break;
						case 7:
							wan.subnetmask = "254.0.0.0";
							break;
						case 8:
							wan.subnetmask = "255.0.0.0";
							break;
						case 9:
							wan.subnetmask = "255.128.0.0";
							break;
						case 10:
							wan.subnetmask = "255.192.0.0";
							break;
						case 11:
							wan.subnetmask = "255.224.0.0";
							break;
						case 12:
							wan.subnetmask = "255.240.0.0";
							break;
						case 13:
							wan.subnetmask = "255.248.0.0";
							break;
						case 14:
							wan.subnetmask = "255.252.0.0";
							break;
						case 15:
							wan.subnetmask = "255.254.0.0";
							break;
						case 16:
							wan.subnetmask = "255.255.0.0";
							break;
						case 17:
							wan.subnetmask = "255.255.128.0";
							break;
						case 18:
							wan.subnetmask = "255.255.192.0";
							break;
						case 19:
							wan.subnetmask = "255.255.224.0";
							break;
						case 20:
							wan.subnetmask = "255.255.240.0";
							break;
						case 21:
							wan.subnetmask = "255.255.248.0";
							break;
						case 22:
							wan.subnetmask = "255.255.252.0";
							break;
						case 23:
							wan.subnetmask = "255.255.254.0";
							break;
						case 24:
							wan.subnetmask = "255.255.255.0";
							break;
						case 25:
							wan.subnetmask = "255.255.255.128";
							break;
						case 26:
							wan.subnetmask = "255.255.255.192";
							break;
						case 27:
							wan.subnetmask = "255.255.255.224";
							break;
						case 28:
							wan.subnetmask = "255.255.255.240";
							break;
						case 29:
							wan.subnetmask = "255.255.255.248";
							break;
						case 30:
							wan.subnetmask = "255.255.255.252";
							break;
						case 31:
							wan.subnetmask = "255.255.255.254";
							break;
						case 32:
							wan.subnetmask = "255.255.255.255";
							break;
						default:
							wan.subnetmask = "";
							break;
					}
				}
			}
			return wan;
		}


		/// <summary>
		/// Возвращает сколько скачано и отдано и скорость.
		/// </summary>
		/// <param name="modem"></param>
		/// <returns></returns>
		public static CModemTrafficStats GetTrafficStats(CMManagerBearerInfoBearer bearer, CModemTrafficStats trafficStats)
		{
			CModemTrafficStats result = new CModemTrafficStats 
			{
				bytesTx = 0,
				bytesRx = 0,
				speedBpsTx = 0,
				speedBpsRx = 0,
				time = DateTime.Now
			};

			if (bearer != null)
			{
				if (bearer.stats.bytesTx != "--") result.bytesTx = Convert.ToInt64(bearer.stats.bytesTx);
				if (bearer.stats.bytesTx != "--") result.bytesRx = Convert.ToInt64(bearer.stats.bytesRx);

				if (trafficStats != null && trafficStats.bytesTx > 0 && trafficStats.bytesRx > 0)
				{
					// получим разницу в секундах от последнего результата
					int sec = Convert.ToInt32(Math.Abs((result.time - trafficStats.time).TotalSeconds));
					if (sec > 5)
					{
						long deltaTx = Math.Abs(result.bytesTx - trafficStats.bytesTx);
						long deltaRx = Math.Abs(result.bytesRx - trafficStats.bytesRx);

						result.speedBpsTx = Convert.ToInt64(deltaTx / sec);
						result.speedBpsRx = Convert.ToInt64(deltaRx / sec);
					}
					else
					{
						result = trafficStats;
					}
				}

			}
			return result;
		}

		/// <summary>
		/// Получает баланс SIM карты через USSD запрос.
		/// </summary>
		/// <param name="modem"></param>
		/// <returns></returns>
		public static async Task<double?> GetBalanceByUSSD(CModem modem)
		{
			// mmcli -m 0 --3gpp-ussd-initiate=*100#
			double? result = null;
			List<string> outputCmd = new List<string>();
			CommandResult resCmd = null;
			try
			{
				resCmd = await CommandShell.Run("/usr/bin/mmcli", new string[] { $"--modem=" + modem.modemManagerId.ToString(), $"--3gpp-ussd-initiate={modem.settingOperator.ussdBalance}" }).RedirectTo(outputCmd).Task;
			}
			catch { }
			if (resCmd != null && resCmd.Success)
			{
				string strT = string.Join("", outputCmd);
				// USSD session initiated; new reply from network: '813.33 р. Вместо гудка хит Attention от Charlie Puth за 6 руб./ день: *770 * 70#'
				// USSD session initiated; new reply from network: '-386.67 р.
				Match match = regexBalanceMegafon.Match(strT);
				if (match.Success) result = Convert.ToDouble(match.Groups[1].Value.Replace(".", ",").Trim());

			}
			return result;
		}

		/// <summary>
		/// Получает баланс SIM карты через Web кабинет.
		/// </summary>
		/// <param name="modem"></param>
		/// <returns></returns>
		public static async Task<double?> GetBalanceByWeb(CModem modem)
		{
			// https://4pda.to/forum/index.php?showtopic=985296&st=80
			// https://github.com/dubanoze/beeapi

			if (modem.simOptions != null && modem.simOptions.mode == "web" && IsNotNull(modem.simOptions.login) && IsNotNull(modem.simOptions.password))
			{
				try
				{
					if (modem.operatorName == "Beeline")
					{
						// Получим токен авторизации
						RestClient client = new RestClient("https://my.beeline.ru");
						client.Timeout = 10000;
						client.ConfigureWebRequest((r) =>
						{
							r.ServicePoint.Expect100Continue = false;
							r.KeepAlive = true;
							r.ServicePoint.BindIPEndPointDelegate = (servicePoint, remoteEndPoint, retryCount) => IPEndPoint.Parse(configuration["ServerIp"]);
						});
						var options = new JsonSerializerOptions();
						options.Converters.Add(new ESDateTimeConverter());
						client.UseSystemTextJson(options);
						RestRequest request = new RestRequest($"/api/1.0/auth/auth?login={modem.simOptions.login}&password={modem.simOptions.password}", DataFormat.Json);
						request.AddHeader("Accept", "application/json");
						CBalanceBeelineApiAuth reqAuth = await client.GetAsync<CBalanceBeelineApiAuth>(request, CancellationToken.None);
						if (reqAuth != null && reqAuth.meta.status == "OK" && IsNotNull(reqAuth.token))
						{
							request = new RestRequest($"/api/1.0/info/postpaidBalance?ctn={modem.simOptions.login}&token={reqAuth.token}", DataFormat.Json);
							request.AddHeader("Accept", "application/json");
							CBalanceBeelineApiPostpaidBalance reqBalance = await client.GetAsync<CBalanceBeelineApiPostpaidBalance>(request, CancellationToken.None);
							if (reqBalance != null && reqBalance.meta.status == "OK") return reqBalance.balance;
						}
					}
				}
				catch { }
			}

			return null;
		}

		public static void ChangeModeBalance(string simIccid, string balanceMode, string webLogin = null, string webPassword = null)
		{
			//lock (lockerDb)
			if (true)
			{
				using (LiteDB.LiteDatabase db = new LiteDB.LiteDatabase("modems.db"))
				{
					LiteDB.ILiteCollection<CSimOptions> colSim = db.GetCollection<CSimOptions>("sim_options");
					colSim.EnsureIndex(x => x.iccid);

					CSimOptions findSim = colSim.FindOne(x => x.iccid.Contains(simIccid));
					if (findSim != null)
					{
						findSim.mode = balanceMode;
						findSim.login = webLogin;
						findSim.password = webPassword;
						colSim.Update(findSim);
					}
					else
					{
						colSim.Insert(new CSimOptions { iccid = simIccid, mode = balanceMode, login = webLogin, password = webPassword });
					}
				}
			}
		}

		/// <summary>
		/// Возвращает информацию об уровне сигнала, куда подключено и другая теническая информация.
		/// </summary>
		/// <param name="modem"></param>
		/// <returns></returns>
		public static async Task<CSignalStrength> GetModemSignalStrength(string dev)
		{
			// qmicli -d /dev/cdc-wdm0 -p --nas-get-signal-strength

			CSignalStrength result = new CSignalStrength();
			List<string> outputCmd = new List<string>();
			CommandResult resCmd = null;
			try
			{
				resCmd = await CommandShell.Run("/usr/bin/qmicli", new string[] { $"--device={dev}", "-p", "--nas-get-signal-strength" }).RedirectTo(outputCmd).Task;
			}
			catch { }
			if (resCmd != null && resCmd.Success)
			{
				if (outputCmd.Count > 0)
				{
					for (int i = 0; i < outputCmd.Count; i++)
					{
						if (outputCmd[i].Contains("RSSI:"))
						{
							Match match = regexSignal.Match(outputCmd[i + 1]);
							if (match.Success) result.rssi = Convert.ToDouble(match.Groups[1].Value.Replace("dBm", "").Replace("dB", "").Trim());
						}
						else if (outputCmd[i].Contains("ECIO:"))
						{
							Match match = regexSignal.Match(outputCmd[i + 1]);
							if (match.Success) result.ecio = Convert.ToDouble(match.Groups[1].Value.Replace("dBm", "").Replace("dB", "").Trim());
						}
						else if (outputCmd[i].Contains("IO:"))
						{
							Match match = regexSignal.Match(outputCmd[i]);
							if (match.Success) result.io = Convert.ToDouble(match.Groups[1].Value.Replace("dBm", "").Replace("dB", "").Trim());
						}
						else if (outputCmd[i].Contains("SINR"))
						{
							Match match = regexSignal.Match(outputCmd[i]);
							if (match.Success) result.sinr = Convert.ToDouble(match.Groups[1].Value.Replace("dBm", "").Replace("dB", "").Trim());
						}
						else if (outputCmd[i].Contains("RSRQ:"))
						{
							Match match = regexSignal.Match(outputCmd[i + 1]);
							if (match.Success) result.rsrq = Convert.ToDouble(match.Groups[1].Value.Replace("dBm", "").Replace("dB", "").Trim());
						}
						else if (outputCmd[i].Contains("SNR:"))
						{
							Match match = regexSignal.Match(outputCmd[i + 1]);
							if (match.Success) result.snr = Convert.ToDouble(match.Groups[1].Value.Replace("dBm", "").Replace("dB", "").Trim());
						}
						else if (outputCmd[i].Contains("RSRP:"))
						{
							Match match = regexSignal.Match(outputCmd[i + 1]);
							if (match.Success) result.rsrp = Convert.ToDouble(match.Groups[1].Value.Replace("dBm", "").Replace("dB", "").Trim());
						}
					}
				}
			}


			outputCmd = new List<string>();
			resCmd = null;
			try
			{
				resCmd = await CommandShell.Run("/usr/bin/qmicli", new string[] { $"--device={dev}", "-p", "--nas-get-system-selection-preference" }).RedirectTo(outputCmd).Task;
			}
			catch { }
			if (resCmd != null && resCmd.Success)
			{
				if (outputCmd.Count > 0)
				{
					for (int i = 0; i < outputCmd.Count; i++)
					{
						if (outputCmd[i].Contains("Mode preference:"))
						{
							Match match = regexQuotes.Match(outputCmd[i]);
							if (match.Success) result.mode = match.Groups[1].Value.Trim();
						}
						else if (outputCmd[i].Contains("Band preference:"))
						{
							Match match = regexSignal.Match(outputCmd[i]);
							if (match.Success)
							{
								result.bandPreference = match.Groups[1].Value.Trim().Replace(" ", "").Split(",").ToList();
							}
						}
						else if (outputCmd[i].Contains("LTE band preference:"))
						{
							Match match = regexSignal.Match(outputCmd[i]);
							if (match.Success)
							{
								result.lteBand = new List<int>();
								string lb = match.Groups[1].Value.Trim().Replace(" ", "");
								foreach (string t in lb.Split(","))
								{
									result.lteBand.Add(Convert.ToInt32(t));
								}
							}
						}
					}
				}
			}

			outputCmd = new List<string>();
			resCmd = null;
			try
			{
				resCmd = await CommandShell.Run("/usr/bin/qmicli", new string[] { $"--device={dev}", "-p", "--nas-get-serving-system" }).RedirectTo(outputCmd).Task;
			}
			catch { }
			if (resCmd != null && resCmd.Success)
			{
				if (outputCmd.Count > 0)
				{
					for (int i = 0; i < outputCmd.Count; i++)
					{
						if (outputCmd[i].Contains("Registration state:"))
						{
							Match match = regexQuotes.Match(outputCmd[i]);
							if (match.Success) result.registrationState = match.Groups[1].Value.Trim();
						}
						else if(outputCmd[i].Contains("Selected network:"))
						{
							Match match = regexQuotes.Match(outputCmd[i]);
							if (match.Success) result.network = match.Groups[1].Value.Trim();
						}
						else if (outputCmd[i].Contains("MCC:"))
						{
							Match match = regexQuotes.Match(outputCmd[i]);
							if (match.Success) result.mcc = Convert.ToInt32(match.Groups[1].Value.Trim());
						}
						else if (outputCmd[i].Contains("MNC:"))
						{
							Match match = regexSignal.Match(outputCmd[i]);
							if (match.Success) result.mnc = Convert.ToInt32(match.Groups[1].Value.Trim());
						}
						else if (outputCmd[i].Contains("Description:"))
						{
							Match match = regexSignal.Match(outputCmd[i]);
							if (match.Success) result.nameOperator = match.Groups[1].Value.Trim();
						}
						else if (outputCmd[i].Contains("3GPP cell ID:"))
						{
							Match match = regexSignal.Match(outputCmd[i]);
							if (match.Success) result.cellId = match.Groups[1].Value.Trim();
						}
						else if (outputCmd[i].Contains("LTE tracking area code:"))
						{
							Match match = regexSignal.Match(outputCmd[i]);
							if (match.Success) result.trackingAreaCode = match.Groups[1].Value.Trim();
						}
						else if (outputCmd[i].Contains("location area code:"))
						{
							Match match = regexSignal.Match(outputCmd[i]);
							if (match.Success) result.locationAreaCode = match.Groups[1].Value.Trim();
						}
					}
				}
			}
			return result;
		}

	}
}
