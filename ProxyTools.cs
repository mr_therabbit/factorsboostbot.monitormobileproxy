﻿using FactorsBoostBot.MonitorMobileProxy.CTypes;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FactorsBoostBot.MonitorMobileProxy
{
	public class ProxyTools
	{
		private static ProxyTools instance;
        private IConfiguration configuration { get; set; }
        private bool isNeedUpdateConf { get; set; } = false;
        private List<CUse3ProxyPort> use3ProxyPorts { get; set; } // Список портов 3proxy которые используются в данный момент
        private Dictionary<string, string> portByProvider { get; set; }

        private ProxyTools()
        {
        }

        public static ProxyTools GetInstance()
        {
            if (instance == null) instance = new ProxyTools();
            return instance;
        }

        public void SetConfiguration(IConfiguration configuration)
        {
            if (this.configuration == null) this.configuration = configuration;

            portByProvider = new Dictionary<string, string>();
            this.configuration.GetSection("ModemsPorts").Bind(portByProvider);

            // Составим список используемых портов 3proxy
            if (use3ProxyPorts == null)
            {
                use3ProxyPorts = new List<CUse3ProxyPort>();
                foreach (KeyValuePair<string, string> p in portByProvider)
                {
                    use3ProxyPorts.Add(new CUse3ProxyPort
                    {
                        port = Convert.ToInt32(p.Key),
                        ip = null,
                        operatorName = p.Value,
                        isUse = false,
                        modemId = null
                    });
                }
            }
        }

        /// <summary>
        /// Возвращает порт для данного ip и оператора.
        /// Если порт уже используется этим ip, то возвращает его.
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="operatorName"></param>
        public int GetFreePort(string ip, string operatorName, string modemId)
		{
            // Может такой ip уже используется?
            CUse3ProxyPort port = use3ProxyPorts.Where(p => p.isUse == true && p.ip == ip).FirstOrDefault();
            if (port != null) return port.port;

            // Ищем не используемый порт
            port = use3ProxyPorts.Where(p => p.isUse == false && p.operatorName.ToLower() == operatorName.ToLower()).FirstOrDefault();
            if (port != null)
            {
                port.isUse = true;
                port.ip = ip;
                port.modemId = modemId;
                isNeedUpdateConf = true;
                return port.port;
            }

            return 0;
        }

        /// <summary>
        /// Освобождает порт.
        /// </summary>
        /// <param name="port"></param>
        public void RemovePort(int port)
        {
            var t = use3ProxyPorts.Where(p => p.port == port).FirstOrDefault();
            if (t != null)
            {
                t.ip = null;
                t.isUse = false;
                t.modemId = null;
                isNeedUpdateConf = true;
            }
        }

        /// <summary>
        /// Сменяет у порта IP.
        /// </summary>
        /// <param name="port"></param>
        /// <param name="ip"></param>
        public void ChangeIpPort(int port, string ip, string modemId)
        {
            var t = use3ProxyPorts.Where(p => p.port == port).FirstOrDefault();
            if (t != null)
            {
                t.ip = ip;
                t.isUse = true;
                t.modemId = modemId;
                isNeedUpdateConf = true;
            }
        }

        /// <summary>
        /// Освобождает порты, для не активных модемов
        /// </summary>
        public void RemoveDeactivePorts()
		{
            ListModems listModems = ListModems.GetInstance();

            // Пробежимся по использованным портам
            foreach (CUse3ProxyPort port in use3ProxyPorts)
			{
                if (port.isUse && port.port > 0)
				{
                    // Найдем модем, который использует этот порт
                    CModem modem = listModems.GetList().Where(p => p.port3proxy == port.port).FirstOrDefault();
                    // Данный используемый порт ни один модем не используем, помечаем его, что он не используется
                    if (modem == null) RemovePort(port.port);
                }
			}

            // Пробежимся по модемам
            // Проверим, правильные ли у портов ip и нужно ли их исправить на ip модема
            foreach (CModem modem in listModems.GetList())
			{
                if (modem.wan != null && !string.IsNullOrWhiteSpace(modem.wan.ip))
				{
                    // Найдем порт для этого модема
                    CUse3ProxyPort port = use3ProxyPorts.Where(p => p.modemId == modem.uid).FirstOrDefault();
                    // Порт есть и ip у порта и модема отличаются
                    if (port != null && modem.wan.ip != port.ip)
					{
                        ChangeIpPort(port.port, modem.wan.ip, modem.uid);
                    }
                }
            }
        }

        /// <summary>
        /// Обновляет конфиг 3proxy.
        /// </summary>
        /// <returns></returns>
        public async Task Update3Proxy()
		{
            Console.WriteLine("Update3Proxy");

            // Деактивируем неиспользуемые порты модемами
            RemoveDeactivePorts();

            if (!isNeedUpdateConf) return;

            List<string> socksProxyConf = new List<string>();
            foreach (CUse3ProxyPort port in use3ProxyPorts)
			{
                if (port.isUse) socksProxyConf.Add($"socks -n -a -p{port.port} -i{configuration["ServerIp"]} -e{port.ip}");
            }
            socksProxyConf.Add("");

            Console.WriteLine($"Изменяем конфиг 3proxy {configuration["Config3proxy"]}");

            await File.WriteAllTextAsync(configuration["Config3proxy"], @$"#!/bin/3proxy

pidfile /var/run/3proxy/3proxy.pid
#chroot /usr/local/3proxy proxy proxy
daemon
config /etc/3proxy/3proxy.cfg
monitor /etc/3proxy/3proxy.cfg

nscache 65536
#nserver 195.208.4.1
#nserver 195.208.5.1
nserver 8.8.8.8
nserver 8.8.4.4

flush
auth none
allow *
{string.Join("\n", socksProxyConf)}
end
".Replace("\r", ""));
            // Перезапускаем 3proxy

            isNeedUpdateConf = false;
        }
    }
}
