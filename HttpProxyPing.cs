﻿using FactorsBoostBot.MonitorMobileProxy.CTypes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Console = Colorful.Console;

namespace FactorsBoostBot.MonitorMobileProxy
{
	public class HttpProxyPing
	{
		private static readonly string[] listPingUrls = new[] { "https://google.com", "https://stackoverflow.com", "https://www.bing.com", "https://www.youtube.com", "https://mail.google.com", "https://mail.ru", "https://www.rambler.ru" };
		private static Random rand { get; set; }
		private static Dictionary<int, HttpClient> clients { get; set; }

		public static async Task<CResponsePingHost> Start(IPAddress ipIface, IPAddress ipProxy, int portProxy, string url = null, int cntGoodPing = 0, int cntBadPing = 0)
		{
			if (ipProxy == null || portProxy == 0) return null;

			if (rand == null) rand = new RandomEx();

			if (string.IsNullOrEmpty(url)) url = listPingUrls[rand.Next(0, listPingUrls.Length)];
			if (clients == null) clients = new Dictionary<int, HttpClient>();

			CResponsePingHost result = new CResponsePingHost
			{
				result = 6,
				bytes = 0,
				time = 0,
				ipServer = null,
				checkDate = DateTime.Now,
				cntGoodPing = 0,
				cntBadPing = 0
			};


			if (!clients.ContainsKey(portProxy))
			{
				HttpClientHandler handler = new HttpClientHandler
				{
					Proxy = new WebProxy($"socks5://{ipProxy}:{portProxy}")
				};

				clients[portProxy] = new HttpClient(handler);
				clients[portProxy].Timeout = TimeSpan.FromSeconds(10);
			}

			//HttpClient client = clients[portProxy];

			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();

			Console.WriteLine($"Пинг с интерфейса {ipIface} через прокси {ipProxy}:{portProxy} на {url}: начался", Color.Yellow);
			HttpResponseMessage res = null;
			try
			{
				res = await clients[portProxy].SendAsync(new HttpRequestMessage(HttpMethod.Head, url));
				if (res.IsSuccessStatusCode)
				{
					result.result = 0;
					result.cntGoodPing = cntGoodPing + 1;
					result.cntBadPing = 0;
				}
			}
			catch (Exception ex)
			{
				result.result = 1;
				result.message = ex.Message;
				result.cntBadPing = cntBadPing + 1;
				
			}

			stopwatch.Stop();
			result.time = stopwatch.Elapsed.TotalMilliseconds;

			if (result.result == 0) Console.WriteLine($"Пинг с интерфейса {ipIface} через прокси {ipProxy}:{portProxy}: время {result.time}мс, удачно", Color.Green);
			else Console.WriteLine($"Пинг с интерфейса {ipIface} через прокси {ipProxy}:{portProxy}: время {result.time}мс, ошибка {result.message}", Color.Red);

			return result;
		}
	}
}
