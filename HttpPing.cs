﻿using FactorsBoostBot.MonitorMobileProxy.CTypes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading.Tasks;
using Console = Colorful.Console;

namespace FactorsBoostBot.MonitorMobileProxy
{
	public class HttpPing
	{
		private static readonly string[] listPingUrls = new[] { "https://google.com", "https://stackoverflow.com", "https://www.bing.com", "https://www.youtube.com", "https://mail.google.com", "https://mail.ru", "https://www.rambler.ru" };
		private static Random rand { get; set; }
		private static Dictionary<IPAddress, SocketsHttpHandler> handlers { get; set; }
		private static Dictionary<IPAddress, HttpClient> clients { get; set; }
		public static async Task<CResponsePingHost> Start(IPAddress ipIface, string url = null, int cntGoodPing = 0, int cntBadPing = 0)
		{
			if (rand == null) rand = new RandomEx();

			if (string.IsNullOrEmpty(url)) url = listPingUrls[rand.Next(0, listPingUrls.Length)];
			if (handlers == null) handlers = new Dictionary<IPAddress, SocketsHttpHandler>();
			if (clients == null) clients = new Dictionary<IPAddress, HttpClient>();

			CResponsePingHost result = new CResponsePingHost
			{
				result = 6,
				bytes = 0,
				time = 0,
				ipServer = null,
				checkDate = DateTime.Now,
				cntGoodPing = 0,
				cntBadPing = 0
			};


			if (!clients.ContainsKey(ipIface))
			{
				if (!handlers.ContainsKey(ipIface))
				{
					handlers[ipIface] = new SocketsHttpHandler();
					handlers[ipIface].ConnectTimeout = TimeSpan.FromSeconds(10);
					handlers[ipIface].AllowAutoRedirect = true;

					handlers[ipIface].ConnectCallback = async (context, cancellationToken) =>
					{
						Socket socket = new Socket(SocketType.Stream, ProtocolType.Tcp);

						socket.Bind(new IPEndPoint(ipIface, 0));

						socket.NoDelay = true;

						try
						{
							await socket.ConnectAsync(context.DnsEndPoint, cancellationToken).ConfigureAwait(false);

							return new NetworkStream(socket, true);
						}
						catch
						{
							socket.Dispose();

							throw;
						}
					};
				}

				clients[ipIface] = new HttpClient(handlers[ipIface]);
				clients[ipIface].Timeout = TimeSpan.FromSeconds(10);
			}

			HttpClient client = clients[ipIface];

			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();

			Console.WriteLine($"Пинг с интерфейса {ipIface} на {url}: начался", Color.Yellow);
			HttpResponseMessage res = null;
			try
			{
				res = await client.SendAsync(new HttpRequestMessage(HttpMethod.Head, url));
				if (res.IsSuccessStatusCode)
				{
					result.result = 0;
					result.cntGoodPing = cntGoodPing + 1;
					result.cntBadPing = 0;
				}
			}
			catch (Exception ex)
			{
				result.result = 1;
				result.message = ex.Message;
				result.cntBadPing = cntBadPing + 1;
				
			}

			stopwatch.Stop();
			result.time = stopwatch.Elapsed.TotalMilliseconds;

			if (result.result == 0) Console.WriteLine($"Пинг с интерфейса {ipIface} на {url}: время {result.time}мс, удачно", Color.Green);
			else Console.WriteLine($"Пинг с интерфейса {ipIface} на {url}: время {result.time}мс, ошибка {result.message}", Color.Red);

			return result;
		}
	}
}
