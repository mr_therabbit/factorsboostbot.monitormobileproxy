﻿using FactorsBoostBot.MonitorMobileProxy.CTypes;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Console = Colorful.Console;

namespace FactorsBoostBot.MonitorMobileProxy
{
	public class PingTools
	{
		//Declare some Constant Variables
		const int SOCKET_ERROR = -1;
		const int ICMP_ECHO = 8;

		private static ManualResetEvent waitHandleSendTo { get; set; }
		private static ManualResetEvent waitHandleReceiveFrom { get; set; }
		private static string responseSendTo { get; set; }
		private static string responseReceiveFrom { get; set; }

		/// <summary>
		///		This method takes the "hostname" of the server
		///		and then it ping's it and shows the response time
		/// </summary>
		public static async Task<CResponsePingHost> PingHost(string host, IPAddress ipIface, int cntGoodPing = 0, int cntBadPing = 0)
		{
			CResponsePingHost result = new CResponsePingHost
			{
				result = 6,
				bytes = 0,
				time = 0,
				ipServer = null,
				checkDate = DateTime.Now,
				cntGoodPing = cntGoodPing,
				cntBadPing = cntBadPing + 1
			};
			//Declare the IPHostEntry 
			IPHostEntry serverHE;
			int nBytes = 0;
			//Initilize a Socket of the Type ICMP
			Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Raw, ProtocolType.Icmp);
			socket.SendTimeout = 1000;
			socket.ReceiveTimeout = 1000;

			waitHandleSendTo = new ManualResetEvent(false);

			Console.WriteLine($"Пинг с интерфейса {ipIface} на {host}: начался", Color.Yellow);


			// Get the server endpoint
			try
			{
				serverHE = await Dns.GetHostEntryAsync(host);
			}
			catch (Exception)
			{
				result.result = 5;
				//Console.WriteLine("Host not found"); // fail
				return result;
			}

			// Convert the server IP_EndPoint to an EndPoint
			IPEndPoint ipServer = new IPEndPoint(serverHE.AddressList[0], 0);
			result.ipServer = ipServer.ToString();
			//result.ipServer = new IPEndPoint(IPAddress.Parse("123.2.4.1"), 0);
			EndPoint epServer = (ipServer);

			IPEndPoint ipEndPointFrom = new IPEndPoint(ipIface, 5555);
			EndPoint EndPointFrom = (ipEndPointFrom);
			socket.Bind(ipEndPointFrom);


			int PacketSize = 0;
			IcmpPacket packet = new IcmpPacket();
			// Construct the packet to send
			packet.Type = ICMP_ECHO; //8
			packet.SubCode = 0;
			packet.CheckSum = ushort.Parse("0");
			packet.Identifier = ushort.Parse("45");
			packet.SequenceNumber = ushort.Parse("0");
			int PingData = 32; // sizeof(IcmpPacket) - 8;
			packet.Data = new byte[PingData];
			//Initilize the Packet.Data
			for (int i = 0; i < PingData; i++)
			{
				packet.Data[i] = (byte)'#';
			}

			//Variable to hold the total Packet size
			PacketSize = PingData + 8;
			byte[] icmp_pkt_buffer = new byte[PacketSize];
			int Index = 0;
			//Call a Methos Serialize which counts
			//The total number of Bytes in the Packet
			Index = Serialize(
						  packet,
						  icmp_pkt_buffer,
						  PacketSize,
						  PingData);
			//Error in Packet Size
			if (Index == -1)
			{
				//Console.WriteLine("Error in Making Packet");
				return result;
			}

			// now get this critter into a ushort array

			//Get the Half size of the Packet
			double double_length = Convert.ToDouble(Index);
			double dtemp = Math.Ceiling(double_length / 2);
			int cksum_buffer_length = Convert.ToInt32(dtemp);
			//Create a byte Array
			ushort[] cksum_buffer = new ushort[cksum_buffer_length];
			//Code to initilize the ushort array 
			int icmp_header_buffer_index = 0;
			for (int i = 0; i < cksum_buffer_length; i++)
			{
				cksum_buffer[i] = BitConverter.ToUInt16(icmp_pkt_buffer, icmp_header_buffer_index);
				icmp_header_buffer_index += 2;
			}
			//Call a method which will return a checksum             
			ushort u_cksum = checksum(cksum_buffer, cksum_buffer_length);
			//Save the checksum to the Packet
			packet.CheckSum = u_cksum;

			// Now that we have the checksum, serialize the packet again
			byte[] sendbuf = new byte[PacketSize];
			//again check the packet size
			Index = Serialize(
						packet,
						sendbuf,
						PacketSize,
						PingData);
			//if there is a error report it
			if (Index == -1)
			{
				result.result = 3;
				//Console.WriteLine("Error in Making Packet");
				return result;
			}

			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();

			//responseSendTo = "Timeout";
			//using (SocketAsyncEventArgs argsSendTo = new SocketAsyncEventArgs())
			//{
			//	argsSendTo.RemoteEndPoint = epServer;
			//	argsSendTo.SocketFlags = 0;
			//	argsSendTo.UserToken = waitHandleSendTo;
			//	argsSendTo.SetBuffer(sendbuf, 0, PacketSize);
			//	argsSendTo.Completed += new EventHandler<SocketAsyncEventArgs>(delegate (object s, SocketAsyncEventArgs e)
			//	{
			//		responseSendTo = e.SocketError.ToString();
			//		waitHandleSendTo.Set();
			//	});

			//	if (socket.SendToAsync(argsSendTo))
			//	{
			//		waitHandleSendTo.WaitOne(1000);
			//	}
			//}

			if ((nBytes = socket.SendTo(sendbuf, PacketSize, 0, epServer)) == SOCKET_ERROR)
			{
				result.result = 4;
				//Console.WriteLine("Socket Error cannot Send Packet");
			}
			// Initialize the buffers. The receive buffer is the size of the
			// ICMP header plus the IP header (20 bytes)
			byte[] ReceiveBuffer = new byte[256];
			//Receive the bytes
			bool recd = false;

			//loop for checking the time of the server responding 
			while (!recd)
			{
				//responseReceiveFrom = "Timeout";
				//waitHandleReceiveFrom = new ManualResetEvent(false);
				//using (SocketAsyncEventArgs argsReceiveFrom = new SocketAsyncEventArgs())
				//{
				//	argsReceiveFrom.RemoteEndPoint = epServer;
				//	argsReceiveFrom.SocketFlags = 0;
				//	argsReceiveFrom.UserToken = waitHandleReceiveFrom;
				//	argsReceiveFrom.SetBuffer(ReceiveBuffer, 0, ReceiveBuffer.Length);
				//	argsReceiveFrom.Completed += new EventHandler<SocketAsyncEventArgs>(delegate (object s, SocketAsyncEventArgs e)
				//	{
				//		responseReceiveFrom = e.SocketError.ToString();
				//		result.bytes = e.BytesTransferred;
				//		waitHandleReceiveFrom.Set();
				//	});

				//	if (socket.ReceiveFromAsync(argsReceiveFrom))
				//	{
				//		stopwatch.Stop();
				//		result.time = stopwatch.Elapsed.TotalMilliseconds;
				//		if (!waitHandleReceiveFrom.WaitOne(1000))
				//		{
				//			result.result = 2;
				//			result.bytes = SOCKET_ERROR;
				//			Console.WriteLine("Time Out");
				//		}
				//		else
				//		{
				//			result.result = 0;
				//			Console.WriteLine($"Ответ от {epServer}: число байт={result.bytes} время={result.time}мс");
				//		}
				//		recd = true;
				//	}
				//}


				try
				{
					result.bytes = socket.ReceiveFrom(ReceiveBuffer, 256, 0, ref EndPointFrom);
				}
				catch
				{
					result.bytes = SOCKET_ERROR;
				}

				stopwatch.Stop();
				result.time = stopwatch.Elapsed.TotalMilliseconds;

				if (result.bytes == SOCKET_ERROR)
				{
					result.result = 1;
					//Console.WriteLine("Host not Responding");
					recd = true;
					break;
				}
				else if (result.bytes > 0)
				{
					result.result = 0;
					result.cntBadPing = 0;
					//Console.WriteLine($"Ответ от {epServer}: число байт={result.bytes} время={result.time}мс");
					recd = true;
					break;
				}
				if (result.time > 1000)
				{
					result.result = 2;
					//Console.WriteLine("Time Out");
					recd = true;
					break;
				}

			}

			if (result.result == 0) Console.WriteLine($"Пинг с интерфейса {ipIface} на {host}: время {result.time}мс, удачно", Color.Green);
			else Console.WriteLine($"Пинг с интерфейса {ipIface} на {host}: время {result.time}мс, ошибка {result.message}", Color.Red);

			//close the socket
			socket.Close();

			return result;
		}

		/// <summary>
		///  This method get the Packet and calculates the total size 
		///  of the Pack by converting it to byte array
		/// </summary>
		public static int Serialize(IcmpPacket packet, byte[] Buffer, int PacketSize, int PingData)
		{
			int cbReturn = 0;
			// serialize the struct into the array
			int Index = 0;

			byte[] b_type = new byte[1];
			b_type[0] = (packet.Type);

			byte[] b_code = new byte[1];
			b_code[0] = (packet.SubCode);

			byte[] b_cksum = BitConverter.GetBytes(packet.CheckSum);
			byte[] b_id = BitConverter.GetBytes(packet.Identifier);
			byte[] b_seq = BitConverter.GetBytes(packet.SequenceNumber);

			// Console.WriteLine("Serialize type ");
			Array.Copy(b_type, 0, Buffer, Index, b_type.Length);
			Index += b_type.Length;

			// Console.WriteLine("Serialize code ");
			Array.Copy(b_code, 0, Buffer, Index, b_code.Length);
			Index += b_code.Length;

			// Console.WriteLine("Serialize cksum ");
			Array.Copy(b_cksum, 0, Buffer, Index, b_cksum.Length);
			Index += b_cksum.Length;

			// Console.WriteLine("Serialize id ");
			Array.Copy(b_id, 0, Buffer, Index, b_id.Length);
			Index += b_id.Length;

			Array.Copy(b_seq, 0, Buffer, Index, b_seq.Length);
			Index += b_seq.Length;

			// copy the data	        
			Array.Copy(packet.Data, 0, Buffer, Index, PingData);
			Index += PingData;
			if (Index != PacketSize/* sizeof(IcmpPacket)  */)
			{
				cbReturn = -1;
				return cbReturn;
			}

			cbReturn = Index;
			return cbReturn;
		}
		/// <summary>
		///		This Method has the algorithm to make a checksum 
		/// </summary>
		public static ushort checksum(ushort[] buffer, int size)
		{
			int cksum = 0;
			int counter;

			counter = 0;

			while (size > 0)
			{

				ushort val = buffer[counter];

				cksum += Convert.ToInt32(buffer[counter]);
				counter += 1;
				size -= 1;
			}

			cksum = (cksum >> 16) + (cksum & 0xffff);
			cksum += (cksum >> 16);
			return (ushort)(~cksum);
		}
	}

	public class IcmpPacket
	{
		public byte Type;    // type of message
		public byte SubCode;    // type of sub code
		public ushort CheckSum;   // ones complement checksum of struct
		public ushort Identifier;      // identifier
		public ushort SequenceNumber;     // sequence number  
		public byte[] Data;

	}
}
