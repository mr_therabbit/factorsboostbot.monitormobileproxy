﻿using FactorsBoostBot.MonitorMobileProxy.CTypes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Console = Colorful.Console;

namespace FactorsBoostBot.MonitorMobileProxy
{
	public class ListModems
	{
		private static ListModems instance;
		private List<CModem> modems;

		private ListModems()
        {
			modems = new List<CModem>();
		}

        public static ListModems GetInstance()
        {
            if (instance == null) instance = new ListModems();
            return instance;
        }

        public void Set(CModem modem)
		{
			if (!string.IsNullOrWhiteSpace(modem.uid))
			{
				Console.WriteLine($"Модем {modem.dev}: пытаемся обновить список модемов.", Color.Green);
				CModem m = modems.Where(p => p.uid == modem.uid).FirstOrDefault();
				if (m == null)
				{
					Console.WriteLine($"Модем {modem.dev}: добавляем.", Color.Green);
					modems.Add(modem);
				}
				else
				{
					Console.WriteLine($"Модем {modem.dev}: обновляем.", Color.Green);
					modems[modems.IndexOf(m)] = modem;
				}
			}
		}

		public void Remove(CModem modem)
		{
			CModem m = modems.Where(p => p == modem).FirstOrDefault();
			if (m != null) modems.Remove(m);
		}

		public CModem GetById(int id)
		{
			return modems.Where(p => p.id == id).FirstOrDefault();
		}

		public CModem GetByUid(string uid)
		{
			return modems.Where(p => p.uid == uid).FirstOrDefault();
		}

		public List<CModem> GetList()
		{
			return modems;
		}

	}
}
