﻿using FactorsBoostBot.MonitorMobileProxy.CTypes;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Quartz;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Console = Colorful.Console;


namespace FactorsBoostBot.MonitorMobileProxy.Jobs
{
	/// <summary>
	/// Задача - обновление значений баланса сим карт модемов.
	/// </summary>
	// Этот атрибут предотвращает попытки Quartz.NET одновременно выполнять одно и то же задание .
	[DisallowConcurrentExecution]
	public class WatchdogModems : IJob
	{
		private readonly ILogger<ControlModems> logger;
		private readonly IConfiguration configuration;
		private ListModems listModems;

		/// <summary>
		/// Конструктор, который запускается одини раз при запуске приложения.
		/// </summary>
		/// <param name="configuration"></param>
		/// <param name="logger"></param>
		public WatchdogModems(IConfiguration configuration, ILogger<ControlModems> logger)
		{
			this.logger = logger;
			this.configuration = configuration;
			listModems = ListModems.GetInstance();
			Console.WriteLine("Запуск WatchdogModems");
		}

		public async Task CheckPingModem(string uid)
		{
			CModem m = listModems.GetList().Where(p => p.uid == uid && p.active == true).FirstOrDefault();
			try
			{
				if (m != null && m.active && m.wan != null && !string.IsNullOrWhiteSpace(m.wan.ip) && m.state == "connected" && m.balance != null && m.balance > 0)
				{
					//Console.WriteLine($"Делаем пинг с интерфейса {m.wan.ip}", Color.Yellow);
					//m.ping = await PingTools.PingHost("yandex.ru", IPAddress.Parse(m.wan.ip), m.ping != null ? m.ping.cntBadPing : 0);
					//m.ping = await HttpPing.Start(IPAddress.Parse(m.wan.ip), null, m.ping != null ? m.ping.cntBadPing : 0);
					m.ping = await HttpProxyPing.Start(IPAddress.Parse(m.wan.ip), IPAddress.Parse(m.host3proxy), m.port3proxy, null, m.ping != null ? m.ping.cntGoodPing : 0, m.ping != null ? m.ping.cntBadPing : 0);
					//Console.WriteLine($"Ответ от {m.ping.ipServer}: число байт={m.ping.bytes} время={m.ping.time}мс", Color.Green);
					
					if (m.ping.result != 0)
					{
						// Если два пинга подряд были неудачные, то модем нельзя использовать
						if (m.ping.cntBadPing >= 2)
						{
							m.isReadyUse = false;
						}

						//Console.WriteLine($"Неудачный пинг с интерфейса {m.wan.ip}", Color.Red);
						if (m.ping.cntBadPing >= 6)
						{
							m.ping.isNeedReboot = true;
						}
						else if (m.ping.cntBadPing >= 4)
						{
							m.ping.isNeedChangeIp = true;
						}
						else if (m.ping.cntBadPing >= 2)
						{
							m.ping.isNeedChangeDhcp = true;
						}
					}
					else
					{
						m.ping.cntBadPing = 0;
						// Если пингов удачных больше или равно 2х, то модем можно использовать
						if (m.ping.cntGoodPing >= 2) m.isReadyUse = true;
					}

					if (m.ping.isNeedReboot)
					{
						m.ping.isNeedReboot = false;
						m.ping.cntBadPing = 0;
						m.ping.cntGoodPing = 0;
						Console.WriteLine($"Принято решение о перезагрузке модема с интерфейсом {m.wan.ip}", Color.Red);
						listModems.Set(m);
						await ModemTools.Reboot(m);
					}
					else if (m.ping.isNeedChangeIp)
					{
						m.ping.isNeedChangeIp = false;
						m.ping.cntGoodPing = 0;
						Console.WriteLine($"Принято решение о перезапуске содинения с интернетом модема с интерфейсом {m.wan.ip}", Color.Red);
						listModems.Set(m);
						await ModemTools.Reboot(m);
					}
					else if (m.ping.isNeedChangeDhcp)
					{
						m.ping.isNeedChangeDhcp = false;
						m.ping.cntGoodPing = 0;
						Console.WriteLine($"Принято решение о переполучении IP по DHCP, модема с интерфейсом {m.wan.ip}", Color.Red);
						await ModemTools.GetIpByDhcp(m);
						// Данные о можеме изменились, получим их заново
						m = listModems.GetList().Where(p => p.uid == uid && p.active == true).FirstOrDefault();
						if (m != null)
						{
							// Обновим данные о соединении
							CModem modemUpdate = await ModemTools.GetUpdateInfoWanModem(m);
							m.bearer = modemUpdate.bearer;
							m.wan = modemUpdate.wan;
							m.state = modemUpdate.state;
						}
					}

					listModems.Set(m);
				}
			}
			catch { }
		}

		public async Task Execute(IJobExecutionContext context)
		{
			if (listModems.GetList() != null && listModems.GetList().Count > 0)
			{
				try
				{
					List<Task> tasks = new List<Task>();
					foreach (CModem modem in listModems.GetList())
					{
						if (!string.IsNullOrWhiteSpace(modem.uid) && modem.active) tasks.Add(CheckPingModem(modem.uid));
					}

					await Task.WhenAll(tasks);
				}
				catch { }
			}
		}
	}
}
