﻿using FactorsBoostBot.MonitorMobileProxy.CTypes;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FactorsBoostBot.MonitorMobileProxy.Jobs
{
	/// <summary>
	/// Задача - обновление значений баланса сим карт модемов.
	/// </summary>
	// Этот атрибут предотвращает попытки Quartz.NET одновременно выполнять одно и то же задание .
	[DisallowConcurrentExecution]
	public class GetBalanceModems : IJob
	{
		private readonly ILogger<ControlModems> logger;
		private readonly IConfiguration configuration;
		private ListModems listModems;

		/// <summary>
		/// Конструктор, который запускается одини раз при запуске приложения.
		/// </summary>
		/// <param name="configuration"></param>
		/// <param name="logger"></param>
		public GetBalanceModems(IConfiguration configuration, ILogger<ControlModems> logger)
		{
			this.logger = logger;
			this.configuration = configuration;
			listModems = ListModems.GetInstance();
			Console.WriteLine("Запуск GetBalanceModems");
		}

		public async Task CheckBalanceModem(CModem modem)
		{
			CModem m = listModems.GetByUid(modem.uid);
			if (m != null && m.active && m.simOptions != null && !string.IsNullOrEmpty(m.simOptions.mode))
			{
				try
				{
					if (m.simOptions.mode == "ussd")
					{
						m.balance = await ModemTools.GetBalanceByUSSD(m);
						listModems.Set(m);
					}
					else if (m.simOptions.mode == "web")
					{
						m.balance = await ModemTools.GetBalanceByWeb(m);
						listModems.Set(m);
					}
				}
				catch { }
			}
		}

		public async Task Execute(IJobExecutionContext context)
		{
			if (listModems.GetList() != null && listModems.GetList().Count > 0)
			{
				List<Task> tasks = new List<Task>();
				foreach (CModem modem in listModems.GetList())
				{
					if (!string.IsNullOrWhiteSpace(modem.uid) && modem.active) tasks.Add(CheckBalanceModem(modem));
				}
				await Task.WhenAll(tasks);
			}
		}
	}
}
