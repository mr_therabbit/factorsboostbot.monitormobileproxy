﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Quartz;
using System.Collections.Generic;
using System.Threading.Tasks;
using Console = Colorful.Console;

namespace FactorsBoostBot.MonitorMobileProxy.Jobs
{

	// Этот атрибут предотвращает попытки Quartz.NET одновременно выполнять одно и то же задание .
	[DisallowConcurrentExecution]
	public class ControlModems : IJob
	{
		private readonly ILogger<ControlModems> logger;
		private readonly IConfiguration configuration;
		private ProxyTools proxyInstance { get; set; }


		/// <summary>
		/// Конструктор, который запускается каждый раз при запуске задачи.
		/// </summary>
		/// <param name="configuration"></param>
		/// <param name="logger"></param>
		public ControlModems(IConfiguration configuration, ILogger<ControlModems> logger)
		{
			this.logger = logger;
			this.configuration = configuration;

			Console.WriteLine("Запуск ControlModems");

			proxyInstance = ProxyTools.GetInstance();
			proxyInstance.SetConfiguration(configuration);
		}

		public async Task Execute(IJobExecutionContext context)
		{
			List<Task> tasks = new List<Task>();

			foreach (int numModem in await ModemTools.GetListNumModemsFromManager())
			{
				tasks.Add(ModemTools.CheckModem(numModem, configuration));
			}

			await Task.WhenAll(tasks);

			// Правим файл конфига 3proxy
			await proxyInstance.Update3Proxy();
		}
	}
}
