﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FactorsBoostBot.MonitorMobileProxy.CTypes
{
	public class CResponseChangeIp
	{
		public int id { get; set; }
		public string uid { get; set; }
		public string status { get; set; }
		public string ip { get; set; }
		public string extIp { get; set; }
		public string gateway { get; set; }
		public string subnetmask { get; set; }
		public string primaryDns { get; set; }
		public string secondaryDns { get; set; }
		public int? mtu { get; set; }
	}
}
