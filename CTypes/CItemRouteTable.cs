﻿namespace FactorsBoostBot.MonitorMobileProxy.CTypes
{
	public class CItemRouteTable
	{
		public string type { get; set; }
		public string dst { get; set; }
		public string gateway { get; set; }
		public string dev { get; set; }
		public string scope { get; set; }
		public string protocol { get; set; }
		public int metric { get; set; }
		public string[] flags { get; set; }
		public string pref { get; set; }
		public string table { get; set; }
	}

}

