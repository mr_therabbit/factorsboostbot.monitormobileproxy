﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FactorsBoostBot.MonitorMobileProxy.CTypes
{
	public class CResponsePing
	{
		public int id { get; set; }
		public string uid { get; set; }
		public string status { get; set; }
	}
}
