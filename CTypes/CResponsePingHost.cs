﻿using System;
using System.Net;

namespace FactorsBoostBot.MonitorMobileProxy.CTypes
{
	public class CResponsePingHost
	{
		public int result { get; set; } = 6;
		public int bytes { get; set; } = 0;
		public string message { get; set; }
		public double time { get; set; } = 0;
		public string ipServer { get; set; }
		public DateTime checkDate { get; set; } = DateTime.Now;
		public int cntBadPing { get; set; } = 0;
		public int cntGoodPing { get; set; } = 0;
		public bool isNeedChangeIp { get; set; } = false;
		public bool isNeedReboot { get; set; } = false;
		public bool isNeedChangeDhcp { get; set; } = false;
	}
}
