﻿using System;

namespace FactorsBoostBot.MonitorMobileProxy.CTypes
{
	public class CBalanceBeelineApiMeta
	{
		public string status { get; set; }
		public int code { get; set; }
		public string message { get; set; }
	}
	public class CBalanceBeelineApiAuth
	{
		public CBalanceBeelineApiMeta meta { get; set; }
		public string token { get; set; }
	}
	public class CBalanceBeelineApiPostpaidBalance
	{
		public CBalanceBeelineApiMeta meta { get; set; }
		public double balance { get; set; }
		public string currency { get; set; }
		public bool debtExists { get; set; }
		public DateTime? unifiedBillingDay { get; set; }
		public DateTime? nextBillingDate { get; set; }
		public DateTime? periodDate { get; set; }
	}
}
