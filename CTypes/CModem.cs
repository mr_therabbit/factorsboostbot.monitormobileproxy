﻿using System;
using System.Collections.Generic;

namespace FactorsBoostBot.MonitorMobileProxy.CTypes
{
	public class CModemNetwork
	{
		public string ip { get; set; } // ip
		public string extIp { get; set; } // ip
		public string gateway { get; set; }
		public string subnetmask { get; set; }
		public string primaryDns { get; set; }
		public string secondaryDns { get; set; }
		public int? mtu { get; set; }
	}

	public class CSignalStrength
	{
		// https://internetnadachu.com/articles/kakoy-dolzhen-byt-4g-signal/
		/*
		 * RSSI (индикатор уровня полученного сигнала) – это общее название мощности сигнала в беспроводной сети. Этот параметр чаще всего выражается в децибелах (дБ) или в процентах от 1 до 100 и может иметь как отрицательное, так и положительное значение.
		 * Чтобы получить среднее значение уровня сигнала, нужно вычесть шум на линии. Чем выше разница между сигналом и шумом, тем лучше сигнал.
		 */
		// RSSI – представляет всю принимаемую мощность. RSSI меняется в зависимости от активности LTE – чем выше активность передачи данных, тем выше RSSI. Однако это не означает, что конечный пользователь получит более сильный сигнал.
		public double? rssi { get; set; } = null;
		public double? ecio { get; set; } = null;
		public double? io { get; set; } = null;
		// Параметр SINR / SNR показывает отношение сигнал / шум.
		public double? sinr { get; set; } = null;
		public double? snr { get; set; } = null;
		// RSRQ – указывает качество полученного сигнала, его диапазон обычно составляет от -19,5 дБ (плохо) до -3 дБ (хорошо).
		public double? rsrq { get; set; } = null;
		// RSRP – типичный диапазон составляет от -44 дБм (хорошо) до -140 дБм (плохо).
		/*
		 * Существуют стандартные уровни RSRP, позволяющие оценить качество связи:
		 * -30 дБм – максимальный уровень сигнала. Скорее всего, точка доступа / модем находятся очень близко.
		 * -50 дБм – все, что ниже этого уровня, можно рассматривать как отличный сигнал.
		 * -60 дБм – сигнал хороший, надежный.
		 * -67 дБм – минимальное значение для всех услуг, которые требуют бесперебойного и надежного обмена данными.
		 * -70 дБм – сигнал достаточный для выхода в интернет, прочтения электронной почты и новостей.
		 * -80 дБм – минимальный параметр, необходимый для подключения.
		 * -90 дБм –подключение или использование какие-либо сервисов маловероятно.
		 */
		public double? rsrp { get; set; } = null;

		public string registrationState { get; set; }
		public string network { get; set; }
		public int? mcc { get; set; } = null;
		public int? mnc { get; set; } = null;
		public string nameOperator { get; set; }
		public string codeOperator { get; set; }
		public string cellId { get; set; }
		public string trackingAreaCode { get; set; }
		public string locationAreaCode { get; set; }
		public string mode { get; set; }
		public List<string> bandPreference { get; set; }
		public List<int> lteBand { get; set; }

	}

	public class CModemTrafficStats
	{
		public long bytesTx { get; set; } = 0;
		public long bytesRx { get; set; } = 0;
		public long speedBpsTx { get; set; } = 0; // Байты в секунду
		public long speedBpsRx { get; set; } = 0; // Байты в секунду
		public DateTime time { get; set; } = DateTime.Now;
	}

	public class CModem
	{
		public int id { get; set; } // id модема числовой
		public string uid { get; set; } // uid модема строковый
		public int modemManagerId { get; set; } // номер модема в modemManager
		public string imei { get; set; } // IMEI модема
		public string imsi { get; set; } // IMSI сим карты
		public string iccid { get; set; } // iccid сим карты
		public string dev { get; set; } // Путь dev устройства. Напрмиер /dev/cdc-wdm0
		public string wwanIface { get; set; } // Имя интерфейса на котором висит можем. Например wwan0
		public bool active { get; set; } = false; // Признак подключенности
		public bool isReadyUse { get; set; } = false; // Признак что модем можно использовать
		public string state { get; set; } // Статус подключения
		public string city { get; set; }
		public string type { get; set; }
		public string manufacturer { get; set; } // Производитель
		public string model { get; set; } // Модель
		public bool isInitModem { get; set; } = false; // Признак, что мы модем инициализировали
		public bool isNeedGetIpDhcp { get; set; } = false; // Признак, что надо получить IP по DHCP
		public CModemNetwork wan { get; set; }
		public double signalQuality { get; set; } // Уровень сигнала в процентах
		public CSignalStrength signalStrength { get; set; } // Уровень сигнала и инфа о соте
		public CModemTrafficStats trafficStats { get; set; } // Трафик
		public string operatorName { get; set; }
		public string operatorCode { get; set; }
		public string comPort { get; set; }
		public int port3proxy { get; set; } = 0;
		public string host3proxy { get; set; }
		public string revision { get; set; }
		public string numPhone { get; set; }
		public string statusConnect { get; set; }
		public List<int> temperature { get; set; } = null;
		public double? balance { get; set; } = null;
		public CSimOptions simOptions { get; set; }
		public CSettingOperator settingOperator { get; set; }
		public CMManagerBearerInfoBearer bearer { get; set; }
		public CResponsePingHost ping { get; set; }

	}
}
