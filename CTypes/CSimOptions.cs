﻿using LiteDB;

namespace FactorsBoostBot.MonitorMobileProxy.CTypes
{
	public class CSimOptions
	{
		[BsonId]
		public int id { get; set; }
		public string iccid { get; set; }
		public string mode { get; set; }
		public string login { get; set; }
		public string password { get; set; }
	}
}
