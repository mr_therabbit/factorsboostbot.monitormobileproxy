﻿namespace FactorsBoostBot.MonitorMobileProxy.CTypes
{
	public class CUse3ProxyPort
	{
		public int port { get; set; } = 0;
		public string ip { get; set; }
		public string modemId { get; set; } // Для отслеживания, что у модема изменился ip
		public string operatorName { get; set; }
		public bool isUse { get; set; } = false;
	}
}
