﻿using Medallion.Shell;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace FactorsBoostBot.MonitorMobileProxy.CTypes
{
	public class CMManagerModemList
	{
		[JsonPropertyName("modem-list")]
		public List<string> modemList { get; set; }
	}

	public class CMManagerModemInfoModem3gppEpsInitialBearerSettings
	{
		public string apn { get; set; }
		[JsonPropertyName("ip-type")]
		public string ipType { get; set; }
		public string password { get; set; }
		public string user { get; set; }

	}

	public class CMManagerModemInfoModem3gppEpsInitialBearer
	{
		[JsonPropertyName("dbus-path")]
		public string dbusPath { get; set; }

		[JsonPropertyName("settings")]
		public CMManagerModemInfoModem3gppEpsInitialBearerSettings settings { get; set; }

	}

	public class CMManagerModemInfoModem3gppEps
	{
		[JsonPropertyName("initial-bearer")]
		public CMManagerModemInfoModem3gppEpsInitialBearer initialBearer { get; set; }

		[JsonPropertyName("ue-mode-operation")]
		public string ueModeOperation { get; set; }
	}

	public class CMManagerModemInfoModem3gppGenericSignalQuality
	{
		public string recent { get; set; }
		[JsonNumberHandling(JsonNumberHandling.AllowReadingFromString)]
		public double value { get; set; }
	}

	public class CMManagerModemInfoModem3gppGeneric
	{
		[JsonPropertyName("access-technologies")]
		public List<string> accessTechnologies { get; set; }

		[JsonPropertyName("bearers")]
		public List<string> bearers { get; set; }

		[JsonPropertyName("carrier-configuration")]
		public string carrierConfiguration { get; set; }

		[JsonPropertyName("carrier-configuration-revision")]
		public string carrierConfigurationRevision { get; set; }

		[JsonPropertyName("current-bands")]
		public List<string> currentBands { get; set; }

		[JsonPropertyName("current-capabilities")]
		public List<string> currentCapabilities { get; set; }

		[JsonPropertyName("current-modes")]
		public string currentModes { get; set; }

		[JsonPropertyName("device")]
		public string device { get; set; }

		[JsonPropertyName("device-identifier")]
		public string deviceIdentifier { get; set; }

		[JsonPropertyName("drivers")]
		public List<string> drivers { get; set; }

		[JsonPropertyName("equipment-identifier")]
		public string equipmentIdentifier { get; set; }

		[JsonPropertyName("hardware-revision")]
		public string hardwareRevision { get; set; }

		[JsonPropertyName("manufacturer")]
		public string manufacturer { get; set; }

		[JsonPropertyName("model")]
		public string model { get; set; }

		[JsonPropertyName("own-numbers")]
		public List<string> ownNumbers { get; set; }

		[JsonPropertyName("plugin")]
		public string plugin { get; set; }

		[JsonPropertyName("ports")]
		public List<string> ports { get; set; }

		[JsonPropertyName("power-state")]
		public string powerState { get; set; }

		[JsonPropertyName("primary-port")]
		public string primaryPort { get; set; }

		[JsonPropertyName("primary-sim-slot")]
		public string primarySimSlot { get; set; }

		[JsonPropertyName("revision")]
		public string revision { get; set; }

		[JsonPropertyName("signal-quality")]
		public CMManagerModemInfoModem3gppGenericSignalQuality signalQuality { get; set; }

		[JsonPropertyName("sim")]
		public string sim { get; set; }

		//[JsonPropertyName("sim-slots")]
		//public List<string> simSlots { get; set; }

		[JsonPropertyName("state")]
		public string state { get; set; }

		[JsonPropertyName("state-failed-reason")]
		public string stateFailedReason { get; set; }

		[JsonPropertyName("supported-bands")]
		public List<string> supportedBands { get; set; }

		[JsonPropertyName("supported-capabilities")]
		public List<string> supportedCapabilities { get; set; }

		[JsonPropertyName("supported-ip-families")]
		public List<string> supportedIpFamilies { get; set; }

		[JsonPropertyName("supported-modes")]
		public List<string> supportedModes { get; set; }

		[JsonPropertyName("unlock-required")]
		public string unlockRequired { get; set; }

		[JsonPropertyName("unlock-retries")]
		public List<string> unlockRetries { get; set; }
	}

	public class CMManagerModemInfoModem3gppCdma
	{
		[JsonPropertyName("activation-state")]
		public string activationState { get; set; }
		[JsonPropertyName("cdma1x-registration-state")]
		public string cdma1xRegistrationState { get; set; }
		[JsonPropertyName("esn")]
		public string esn { get; set; }
		[JsonPropertyName("evdo-registration-state")]
		public string evdoRegistrationState { get; set; }
		[JsonPropertyName("meid")]
		public string meid { get; set; }
		[JsonPropertyName("nid")]
		public string nid { get; set; }
		[JsonPropertyName("sid")]
		public string sid { get; set; }
	}

	public class CMManagerModemInfoModem3gpp
	{
		[JsonPropertyName("enabled-locks")]
		public List<string> enabledLocks { get; set; }
		public CMManagerModemInfoModem3gppEps eps { get; set; }
		public string imei { get; set; }
		[JsonPropertyName("operator-code")]
		public string operatorCode { get; set; }
		[JsonPropertyName("operator-name")]
		public string operatorName { get; set; }
		public string pco { get; set; }
		[JsonPropertyName("registration-state")]
		public string registrationState { get; set; }
	}

	public class CMManagerModemInfoModem
	{
		[JsonPropertyName("3gpp")]
		public CMManagerModemInfoModem3gpp i3gpp { get; set; }
		public CMManagerModemInfoModem3gppCdma cdma { get; set; }
		[JsonPropertyName("dbus-path")]
		public string dbusPath { get; set; }
		public CMManagerModemInfoModem3gppGeneric generic { get; set; }
	}

	public class CMManagerModemInfo
	{
		public CMManagerModemInfoModem modem { get; set; }
	}

	public class CMManagerSimInfoSimProperties
	{
		[JsonPropertyName("active")]
		public string active { get; set; }

		[JsonPropertyName("eid")]
		public string eid { get; set; }

		[JsonPropertyName("iccid")]
		public string iccid { get; set; }

		[JsonPropertyName("imsi")]
		public string imsi { get; set; }

		[JsonPropertyName("operator-code")]
		public string operatorCode { get; set; }

		[JsonPropertyName("operator-name")]
		public string operatorName { get; set; }

	}

	public class CMManagerSimInfoSim
	{
		[JsonPropertyName("dbus-path")]
		public string dbusPath { get; set; }

		[JsonPropertyName("properties")]
		public CMManagerSimInfoSimProperties properties { get; set; }
	}


	public class CMManagerSimInfo
	{
		public CMManagerSimInfoSim sim { get; set; }
	}

	public class CMManagerBearerInfoBearerIp
	{
		public string address { get; set; }
		public List<string> dns { get; set; }
		public string gateway { get; set; }
		public string method { get; set; }
		public string mtu { get; set; }
		public string prefix { get; set; }
	}

	public class CMManagerBearerInfoBearerProperties
	{
		[JsonPropertyName("allowed-auth")]
		public List<string> allowedAuth { get; set; }
		public string apn { get; set; }

		[JsonPropertyName("ip-type")]
		public string ipType { get; set; }
		public string password { get; set; }

		[JsonPropertyName("rm-protocol")]
		public string rmProtocol { get; set; }
		public string roaming { get; set; }
		public string user { get; set; }
	}

	public class CMManagerBearerInfoBearerStats
	{
		public string attempts { get; set; }
		[JsonPropertyName("bytes-rx")]
		public string bytesRx { get; set; }

		[JsonPropertyName("bytes-tx")]
		public string bytesTx { get; set; }
		public string duration { get; set; }

		[JsonPropertyName("failed-attempts")]
		public string failedAttempts { get; set; }

		[JsonPropertyName("total-bytes-rx")]
		public string totalBytesRx { get; set; }

		[JsonPropertyName("total-bytes-tx")]
		public string totalBytesTx { get; set; }

		[JsonPropertyName("total-duration")]
		public string totalDuration { get; set; }
	}

	public class CMManagerBearerInfoBearerStatus
	{
		public string connected { get; set; }
		public string @interface { get; set; }
		[JsonPropertyName("ip-timeout")]
		public string ipTimeout { get; set; }
		public string suspended { get; set; }
	}

	public class CMManagerBearerInfoBearer
	{
		[JsonPropertyName("dbus-path")]
		public string dbusPath { get; set; }

		[JsonPropertyName("ipv4-config")]
		public CMManagerBearerInfoBearerIp ipv4Config { get; set; }

		[JsonPropertyName("ipv6-config")]
		public CMManagerBearerInfoBearerIp ipv6Config { get; set; }

		[JsonPropertyName("properties")]
		public CMManagerBearerInfoBearerProperties properties { get; set; }

		[JsonPropertyName("stats")]
		public CMManagerBearerInfoBearerStats stats { get; set; }

		[JsonPropertyName("status")]
		public CMManagerBearerInfoBearerStatus status { get; set; }

		[JsonPropertyName("type")]
		public string type { get; set; }
	}

	public class CMManagerBearerInfo
	{
		public CMManagerBearerInfoBearer bearer { get; set; }
	}

	public class CMManagerResponse
	{
		public CommandResult res { get; set; }
		public List<string> output { get; set; }
	}

}
