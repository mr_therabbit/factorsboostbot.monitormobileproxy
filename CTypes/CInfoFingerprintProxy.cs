﻿using System.Collections.Generic;

namespace FactorsBoostBot.MonitorMobileProxy.CTypes
{
	public class CInfoFingerprintProxy
	{
		public string ip { get; set; }
		public string headerUserAgent { get; set; }
		public string detectOS { get; set; }
		public int? mtu { get; set; }
		public int? mss { get; set; }
		public int? ttl { get; set; }

		public string headerSecChUa { get; set; }
		public string headerSecChUaMobile { get; set; }
		public string headerSecChUaPlatform { get; set; }
		public string headerDnt { get; set; }
		public string headerAccept { get; set; }
		public string headerAcceptEncoding { get; set; }
		public string headerAcceptLanguage { get; set; }
		public double? headerDeviceMemory { get; set; }
		public double? headerDownlink { get; set; }
		public double? headerDpr { get; set; }
		public string headerEct { get; set; }
		public double? headerRtt { get; set; }
		public int? headerViewportWidth { get; set; }

		public bool checkProxyXFF { get; set; } = false;
		public string realIpProxyXFF { get; set; }

		public string city { get; set; }
		public double? latitude { get; set; }
		public double? longitude { get; set; }
		public string isp { get; set; }

		public bool threatIsTor { get; set; } = false;
		public bool threatIsProxy { get; set; } = false;
		public bool threatIsAnonymous { get; set; } = false;
		public bool threatIsKnownAttacker { get; set; } = false;
		public bool threatIsKnownAbuser { get; set; } = false;
		public bool threatIsThreat { get; set; } = false;
		public bool threatIsBogon { get; set; } = false;

		public Dictionary<string, string> headers { get; set; }
	}
}
