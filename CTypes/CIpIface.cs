﻿using System.Net.NetworkInformation;

namespace FactorsBoostBot.MonitorMobileProxy.CTypes
{
	public class CIpIface
	{
		public string ifname { get; set; }
		public OperationalStatus status { get; set; }
	}
}
