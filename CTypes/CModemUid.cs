﻿using LiteDB;

namespace FactorsBoostBot.MonitorMobileProxy.CTypes
{
	public class CModemUid
	{
		[BsonId]
		public int id { get; set; }
		public string uid { get; set; }
	}
}
