﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FactorsBoostBot.MonitorMobileProxy.CTypes
{
	public class CSettingOperator
	{
		public string ussdBalance { get; set; }
		public string apn { get; set; }
		public string apnLogin { get; set; }
		public string apnPassword { get; set; }
	}
}
