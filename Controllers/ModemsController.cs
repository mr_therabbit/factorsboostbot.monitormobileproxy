﻿using FactorsBoostBot.MonitorMobileProxy.CTypes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace FactorsBoostBot.MonitorMobileProxy.Controllers
{
	[ApiController]
	[Route("api/modems")]
	public class ModemsController : ControllerBase
	{
		private readonly ILogger<ModemsController> _logger;

		public ModemsController(ILogger<ModemsController> logger)
		{
			_logger = logger;
		}

		/// <summary>
		/// Возвращает список модемов.
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public List<CModem> Get()
		{
			ListModems listModems = ListModems.GetInstance();
			return listModems.GetList();
		}
	}
}
