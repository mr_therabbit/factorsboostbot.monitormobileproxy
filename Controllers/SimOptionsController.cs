﻿using FactorsBoostBot.MonitorMobileProxy.CTypes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace FactorsBoostBot.MonitorMobileProxy.Controllers
{
	[ApiController]
	[Route("api/sim_options")]
	public class SimOptionsController : ControllerBase
	{
		private readonly ILogger<SimOptionsController> _logger;
		private readonly IConfiguration configuration;
		public SimOptionsController(IConfiguration configuration, ILogger<SimOptionsController> logger)
		{
			_logger = logger;
			this.configuration = configuration;
		}

		[HttpGet]
		public async Task<CResponsePing> Get(int id, string mode, string login = null, string password = null)
		{
			CResponsePing result = new CResponsePing { status = "error" };
			// Найдем нужный нам модем
			ListModems listModems = ListModems.GetInstance();
			CModem modem = listModems.GetById(id);
			if (modem != null && !string.IsNullOrEmpty(modem.iccid) && !string.IsNullOrEmpty(mode))
			{
				result.id = modem.id;
				result.uid = modem.uid;

				try
				{
					ModemTools.SetConfiguration(configuration);
					ModemTools.ChangeModeBalance(modem.iccid, mode, login, password);
					result.status = "ok";
				}
				catch
				{
					result.status = "error";
				}
			}

			return result;
		}
	}
}
