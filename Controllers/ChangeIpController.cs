﻿using FactorsBoostBot.MonitorMobileProxy.CTypes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace FactorsBoostBot.MonitorMobileProxy.Controllers
{


	[ApiController]
	[Route("api/change_ip")]
	public class ChangeIpController : ControllerBase
	{
		private readonly ILogger<ChangeIpController> _logger;
		private readonly IConfiguration configuration;
		public ChangeIpController(IConfiguration configuration, ILogger<ChangeIpController> logger)
		{
			_logger = logger;
			this.configuration = configuration;
		}

		[HttpGet]
		public async Task<CResponseChangeIp> Get(int id)
		{
			CResponseChangeIp result = new CResponseChangeIp { status = "error" };
			// Найдем нужный нам модем
			ListModems listModems = ListModems.GetInstance();
			CModem modem = listModems.GetById(id);
			if (modem != null)
			{
				ModemTools.SetConfiguration(configuration);
				modem = await ModemTools.ChangeIp(modem);
				result.status = "ok";
				if (modem != null)
				{
					result.id = modem.id;
					result.uid = modem.uid;
					if (modem.wan != null)
					{
						if (!string.IsNullOrEmpty(modem.wan.ip)) result.ip = modem.wan.ip;
						if (!string.IsNullOrEmpty(modem.wan.extIp)) result.extIp = modem.wan.extIp;
						if (!string.IsNullOrEmpty(modem.wan.gateway)) result.gateway = modem.wan.gateway;
						if (!string.IsNullOrEmpty(modem.wan.subnetmask)) result.subnetmask = modem.wan.subnetmask;
						if (!string.IsNullOrEmpty(modem.wan.primaryDns)) result.primaryDns = modem.wan.primaryDns;
						if (!string.IsNullOrEmpty(modem.wan.secondaryDns)) result.secondaryDns = modem.wan.secondaryDns;
						if (modem.wan.mtu != null) result.mtu = modem.wan.mtu;
					}
				}
			}

			return result;
		}
	}
}
