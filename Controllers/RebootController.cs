﻿using FactorsBoostBot.MonitorMobileProxy.CTypes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FactorsBoostBot.MonitorMobileProxy.Controllers
{

	[ApiController]
	[Route("api/reboot")]
	public class RebootController : ControllerBase
	{
		private readonly ILogger<RebootController> _logger;
		private readonly IConfiguration configuration;
		public RebootController(IConfiguration configuration, ILogger<RebootController> logger)
		{
			_logger = logger;
			this.configuration = configuration;
		}

		[HttpGet]
		public async Task<CResponsePing> Get(int id)
		{
			CResponsePing result = new CResponsePing { status = "error" };
			// Найдем нужный нам модем
			ListModems listModems = ListModems.GetInstance();
			ModemTools.SetConfiguration(configuration);
			CModem modem = listModems.GetById(id);
			if (modem != null)
			{
				result.id = modem.id;
				result.uid = modem.uid;
				try
				{
					await ModemTools.Reboot(modem);
					//await ModemTools.GetIpByDhcp(modem);
					result.status = "ok";
				}
				catch
				{
					result.status = "error";
				}
			}

			return result;
		}
	}
}
