using FactorsBoostBot.MonitorMobileProxy.Jobs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Quartz;
using System;

namespace FactorsBoostBot.MonitorMobileProxy
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			AppContext.SetSwitch("System.Net.DisableIPv6", true);
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{

			services.Configure<QuartzOptions>(options =>
			{
				options.SchedulerName = "Quartz ASP.NET Core Sample Scheduler";
				options.Scheduling.IgnoreDuplicates = true; // default: false
				options.Scheduling.OverWriteExistingData = true; // default: true
			});

			services.AddQuartz(q =>
			{
				// handy when part of cluster or you want to otherwise identify multiple schedulers
				q.SchedulerId = "Scheduler-Core";

				// you can control whether job interruption happens for running jobs when scheduler is shutting down
				q.InterruptJobsOnShutdown = true;

				// when QuartzHostedServiceOptions.WaitForJobsToComplete = true or scheduler.Shutdown(waitForJobsToComplete: true)
				q.InterruptJobsOnShutdownWithWait = true;

				// we can change from the default of 1
				q.MaxBatchSize = 5;

				// we take this from appsettings.json, just show it's possible
				// q.SchedulerName = "Quartz ASP.NET Core Sample Scheduler";

				// this is default configuration if you don't alter it
				q.UseMicrosoftDependencyInjectionJobFactory();

				// these are the defaults
				q.UseSimpleTypeLoader();
				q.UseInMemoryStore();
				q.UseDefaultThreadPool(maxConcurrency: 10);

				q.ScheduleJob<ControlModems>(trigger => trigger
					.WithIdentity("control_modems")
					.StartNow()
					.WithSimpleSchedule(a => a.WithIntervalInSeconds(30).RepeatForever())
					//.WithDailyTimeIntervalSchedule(x => x.WithInterval(30, IntervalUnit.Second))
					.WithDescription("���������� ��������")
				);

				q.ScheduleJob<GetBalanceModems>(trigger => trigger
					.WithIdentity("get_balance_modems")
					.StartNow()
					.WithSimpleSchedule(a => a.WithIntervalInHours(2).RepeatForever())
					.WithDescription("������ ������� �������")
				);

				q.ScheduleJob<WatchdogModems>(trigger => trigger
					.WithIdentity("watchdog_modems")
					.StartNow()
					.WithSimpleSchedule(a => a.WithIntervalInSeconds(10).RepeatForever())
					.WithDescription("�������� ���������� �������")
				);

				//q.UseTimeZoneConverter();

			});


			services.AddQuartzHostedService(options =>
			{
				// when shutting down we want jobs to complete gracefully
				options.WaitForJobsToComplete = true;
			});

			services.AddControllers();

			services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

			services.Configure<ForwardedHeadersOptions>(options =>
			{
				options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseForwardedHeaders();

			app.UseRouting();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
